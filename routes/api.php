<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::resource('detallepedido','detalleapiController', ['only'=>['index','store','show','update','destroy']]);

Route::resource('municipios','municipioController', ['only'=>['index','store','show','update','destroy','getMunicipios']]);


Route::resource('detallespedidosapi','apiDetallesController', ['only'=>['index','store','show','update','destroy']]);


Route::resource('apiUsuario','apiUsuarioController', ['only'=>['index','store','show','update','destroy']]);

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
