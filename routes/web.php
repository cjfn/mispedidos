<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware' => 'auth'], function () {



Route::get('/home', 'HomeController@index');

Route::get('proveedores', 'proveedoresController@index');
Route::post('proveedores', 'proveedoresController@add');
Route::get('proveedores/view', 'proveedoresController@view');
Route::post('proveedores/update', 'proveedoresController@update');
Route::post('proveedores/delete', 'proveedoresController@delete');

Route::get('usuarios', 'userController@index');
Route::post('usuarios', 'userController@add');
Route::get('usuarios/view', 'userController@view');
Route::post('usuarios/update', 'userController@update');
Route::post('usuarios/myupdate', 'userController@myupdate');
Route::post('usuarios/delete', 'userController@delete');
Route::post('usuarios/myupdate', 'userController@myupdate');

Route::get('pedidos', 'pedidoController@index');
Route::post('pedidos', 'pedidoController@add');
Route::get('pedidos/view', 'pedidoController@view');
Route::post('pedidos/update', 'pedidoController@update');
Route::post('pedidos/updateCerrar', 'pedidoController@updateCerrar');
Route::post('pedidos/delete', 'pedidoController@delete');

Route::get('detallespedidos', 'detallepedidosController@index');
Route::post('detallespedidos', 'detallepedidosController@add');
Route::get('detallespedidos/view', 'detallepedidosController@view');
Route::post('detallespedidos/update', 'detallepedidosController@update');
Route::post('detallespedidos/delete', 'detallepedidosController@delete');
Route::resource('detallespedidosn', 'detallepedidosController');

Route::get('productos', 'productosController@index');
Route::post('productos', 'productosController@add');
Route::get('productos/view', 'productosController@view');
Route::post('productos/update', 'productosController@update');
Route::post('productos/delete', 'productosController@deletedetalle');



Route::get('reportesPedidos', 'reportesController@reportesPedidos');
Route::get('reportes', 'reportesController@index');
Route::get('reportesproveedores', 'reportesController@proveedores');
Route::get('reportesdetallespedido', 'reportesController@detallespedido');
Route::post('reportes', 'reportesController@add');
Route::get('reportes/view', 'reportesController@view');
Route::post('reportes/update', 'reportesController@update');
Route::post('reportes/delete', 'reportesController@deletedetalle');

Route::get('municipios/view', 'municipioController@getMunicipios');

Route::resource('apidetallespedidos','apiDetallesPedidosController');

//Route::resource('usuarios', 'userController');


});


Auth::routes();

Route::get('/home', 'HomeController@index');
