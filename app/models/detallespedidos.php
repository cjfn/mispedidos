<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class detallespedidos extends Model
{
    //
     protected $table = 'detallespedidos';
     protected $primarykey='id';
     public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        	
        	'user_id', 
        	'pedido_id',
        	'codigo',
        	'codigo_barras',
        	'nombre',
        	'talla',
        	'color',
        	'cantidad', 
        	'precio_costo',
        	'precio_venta',
        	'user_create',
        	'user_create_id', 
            'activo', 
            'catalogo', 
            'no_pagina',
        	'created_at', 
        	'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];


}
