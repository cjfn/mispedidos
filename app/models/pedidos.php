<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class pedidos extends Model
{
    //
     protected $table = 'pedido';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'user_id',
        'user', 
        'proveedor_id',
        'total', 
        'total_pagado', 
        'aplica_descuento', 
        'porcentaje_descuento', 
        'estado_entrega', 
        'estado_pago', 
        'observaciones', 
        'canal',
        'user_create', 
        'tipo_pago', 
        'activo',
        'checkin',
        'created_at', 
        'updated_at',
        'total_productos'
    ];
}
