<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class productos extends Model
{
    //
    protected $table = 'productos';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'codigo_producto',
        'aplica_descuento', 
        'porcentaje_descuento',
        'nombre', 
        'precio_venta', 
        'precio_compra', 
        'existencia', 
        'factura', 
        'proveedor_id', 
        'fecha_created', 
        'fecha_updated',
        'sucursal', 
        'observaciones', 
        'usuario',
        'idunico',
        'activo'
    ];
}
