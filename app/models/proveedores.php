<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class proveedores extends Model
{
    //
    protected $table = 'proveedores';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'nombre', 'direccion', 'nit', 'contacto', 'telefono', 'correo', 'activo', 'created_at', 'updated_at', 'formula'
    ];
}
