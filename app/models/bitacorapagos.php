<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class bitacorapagos extends Model
{
    //
     protected $table = 'bitacorapagos';
	 protected $primarykey='id';
	 public $timestamps = false;
    
    protected $fillable = [
        'cliente_id',
        'nombre', 
        'pedido',
        'pago', 
        'tipo_pago',
        'created_at', 
        'updated_at'
    ];
}
