<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class municipios extends Model
{
    //
     protected $table = 'municipio';
	 protected $primarykey='id';
	 public $timestamps = false;

     protected $fillable = [
        'nombre_municipio', 'departamento'
    ];

    
}
