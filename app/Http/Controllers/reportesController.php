<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\pedidos;
use App\models\detallespedidos;
use App\models\proveedores;
use App\models\bitacorapagos;
use App\User;
use DB;
use Auth;

class reportesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $pedidos = pedidos::all();
        $user = User::where('tipo_usuario', '=', 'cliente')
         ->where('activo','=', '1')
        ->get();
        $proveedores = proveedores::all();
        return view('reportes.index',['user'=>$user, 'pedidos'=>$pedidos, 'proveedores'=>$proveedores]);
    }

    public function proveedores(Request $request)
    {
        $from= $request->from;
        $to = $request->to;
        $proveedor = $request->proveedor;
        $proveedores = proveedores::find($proveedor);
       
            $data = pedidos::where('activo','=', '1')
            ->orderBy('id','DESC')
            ->where('proveedor_id','=',$proveedor)
            ->whereBetween('created_at', array($from, $to))
            ->get();
        
        $date = date('Y-m-d');
        $mes = $request ->mes;
        $year = $request -> year;

        return view('reportes.proveedores',['data'=>$data,'date'=>$date,'mes'=>$mes, 'year' => $year, 'from'=> $from, 'to'=>$to, 'proveedores'=>$proveedores]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

     public function reportesPedidos(Request $request) 
    {
         $from= $request->from;
        $to = $request->to;
        $cliente = $request->usuario;
        $entrega = $request->entrega;
        $pago = $request->pago;
        if($cliente!='TODOS'&&$pago!='TODOS'&&$entrega!='TODOS')
        {
             $data = pedidos::where('activo','=', '1')
            ->orderBy('id','DESC')
            ->where('user_id','=',$cliente)
            ->where('estado_pago','=',$pago)
            ->where('estado_entrega','=',$entrega)
            ->whereBetween('created_at', array($from, $to))
            ->get();
        }
        else
        {
             if($cliente !='TODOS')
            {
                $data = pedidos::where('activo','=', '1')
                ->orderBy('id','DESC')
                ->where('user_id','=',$cliente)
                ->whereBetween('created_at', array($from, $to))
                ->get();
            }
             else if($pago !='TODOS')
            {
                $data = pedidos::where('activo','=', '1')
                ->orderBy('id','DESC')
                ->where('estado_pago','=',$pago)
                ->whereBetween('created_at', array($from, $to))
                ->get();
            }
             else if($entrega !='TODOS')
            {
                $data = pedidos::where('activo','=', '1')
                ->orderBy('id','DESC')
                ->where('estado_entrega','=',$entrega)
                ->whereBetween('created_at', array($from, $to))
                ->get();
            }
           
            else
            {
                  $data = pedidos::where('activo','=', '1')
                ->orderBy('id','DESC')

                ->whereBetween('created_at', array($from, $to))
                ->get(); 
            }
              
        }
       
        $date = date('Y-m-d');
        $mes = $request ->mes;
        $year = $request -> year;

        return view('reportes.invoice',['data'=>$data,'date'=>$date,'mes'=>$mes, 'year' => $year, 'from'=> $from, 'to'=>$to]);
       
    }

     public function detallespedido(Request $request) 
    {
         $datacount = detallespedidos::where('pedido_id','=', $request->id)->count();
         $data = detallespedidos::where('pedido_id','=', $request->id)
         ->where('activo','=',1)
         ->get();
         $datadetallesp = pedidos::find($request->id);
         $user = User::find($datadetallesp->user_id);
         $datadescuento = proveedores::find($datadetallesp->proveedor_id);
         $pedido_id = $request->id;
         
         $bitacorapagos = bitacorapagos::where('pedido','=',$request->id)
         ->get();
         


        return view('reportes.detallepedidoinvoice',['data'=>$data, 'user' => $user, 'pedido_id'=>$pedido_id,'datacount'=>$datacount, 'datadetallesp'=>$datadetallesp, 'datadescuento'=>$datadescuento,'bitacorapagos'=>$bitacorapagos]);
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
