<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\pedidos;
use App\models\detallepedidos;
use App\models\proveedores;
use App\models\bitacorapagos;
use App\User;
use DB;
use Auth;

class pedidoController extends Controller
{
    //
         public function index()
    {
     if(Auth::user()->tipo_usuario=='admin' || Auth::user()->tipo_usuario=='encargado')
     {
        $dataColaPedido = pedidos::orderBy('id','DESC')
        ->where('activo','=',1)    
            ->get();
        $dataColaProveedor = pedidos::where('activo','=', '1')
            ->where('estado_entrega','=','PEDIDO_PROVEEDOR')
            ->where('activo','=',1)    
            ->orderBy('id','DESC')
            ->get();
        $dataColaBodega = pedidos::where('activo','=', '1')
            ->where('estado_entrega','=','PEDIDO_EN_SUCURSAL')
            ->where('activo','=',1)    
            ->orderBy('id','DESC')
            ->get();
        $dataColaEntregados = pedidos::where('activo','=', '1')
            ->where('estado_entrega','=','PEDIDO_ENTREGADO_CLIENTE')
            ->where('activo','=',1)    
            ->orderBy('id','DESC')
            ->get();          

            $usuarios = User::where('tipo_usuario','=', 'cliente')
         ->where('activo','=', '1')
         ->get();
          $proveedores = DB::table("proveedores")
        ->where('activo','=',1)    
          ->get();
         
            return view('pedidos.index',  ['dataColaProveedor'=>$dataColaProveedor,  'usuarios'=>$usuarios, 'proveedores'=>$proveedores, 'dataColaPedido'=>$dataColaPedido, 'dataColaBodega'=>$dataColaBodega,'dataColaEntregados'=>$dataColaEntregados]);
        }
        else
        {
            $tipo_user = Auth::user()->email;

                  $dataColaPedido = pedidos::where('activo','=', '1')
                ->where('estado_entrega','=','PENDIENTE_PEDIDO')
                ->where('user_create','=', $tipo_user )
                ->orderBy('id','DESC')
                ->get();
        $dataColaProveedor = pedidos::where('activo','=', '1')
            ->where('estado_entrega','=','PEDIDO_PROVEEDOR')
            ->where('user_create','=', $tipo_user )
            ->orderBy('id','DESC')
            ->get();
        $dataColaBodega = pedidos::where('activo','=', '1')
            ->where('estado_entrega','=','PEDIDO_EN_SUCURSAL')
            ->where('user_create','=', $tipo_user )
            ->orderBy('id','DESC')
            ->get();
        $dataColaEntregados = pedidos::where('activo','=', '1')
            ->where('estado_entrega','=','PEDIDO_ENTREGADO_CLIENTE')
            ->orderBy('id','DESC')
            ->where('user_create','=', $tipo_user )
            ->get();          
            $usuarios =  User::where('id','=',Auth::user()->id)
            ->where('activo','=',1)           
            ->get();
          $proveedores = DB::table("proveedores")->get();
         
            return view('pedidos.index',  ['dataColaProveedor'=>$dataColaProveedor,  'usuarios'=>$usuarios, 'proveedores'=>$proveedores, 'dataColaPedido'=>$dataColaPedido, 'dataColaBodega'=>$dataColaBodega,'dataColaEntregados'=>$dataColaEntregados]);
        
        }


    }



   public function add(Request $request)
        {
            $data = new pedidos;
            $data -> user_id = $request -> user_id;
            $usuarios = User::where('id','=', $data -> user_id)
            ->select('name','descuento')
            ->first();
            $data -> user = $usuarios->name;
            $data -> proveedor_id = $request -> proveedor_id;
            $data -> total = 0.0;
            $data -> aplica_descuento = true;
            $data -> porcentaje_descuento = $usuarios->descuento;//despues se calculara solo
            // sumar todo el pagado los pedidos en estado "PAGADOS_TOTALMENTE" de el user_id o cliente y si no esta pendiente de pago se pondra el pedido en inactivo con mensaje de pago pendiente del cliente
            $data -> estado_entrega = "PENDIENTE_PEDIDO";
            $data -> estado_pago = "PENDIENTE_PAGO";
            $data -> observaciones = $request -> observaciones;
            $data -> canal = $request -> canal;
            $data -> user_create =  Auth::user()->email;
            $data -> tipo_pago = $request -> tipo_pago;
            $data -> activo = true;
            $data -> save();
            
            return redirect()->action('detallepedidosController@index', ["id"=>$data->id,"user_id"=>$data->user_id]);
            /*
            return back()
                    ->with('success','Pedido No. '.$data->id.' creado exitosamente.');
                */
                   
                   
                    //return View::make('detallepedidos.index', $data->id);
                    //return view('detallespedidos.index', ["comments"=>$data->id]);
        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                $info = pedidos::find($id);


                /*
                $info = DB::table("pedido as p")
                  ->join ("detallespedidos as dp","p.id","=","dp.pedido_id")
                  ->select('p.id','p.user_id','p.user','p.proveedor_id','p.total','p.total','p.total_pagado','p.aplica_descuento','p.porcentaje_descuento','p.estado_pago','p.estado_entrega','p.observaciones','p.canal','p.user_create','p.tipo_pago','p.activo','p.created_at')
                  ->where('p.id','=', $id)
                 
          ->get();       
          */  
                //echo json_decode($info);
                return response()->json($info);
            }
        }

        public function getproveedores(Request $request)
        {
            if($request->ajax()){
                
                $info = pedidos::all();
                //echo json_decode($info);
                return response()->json($info);
            }
        }

         /*
        *   Update data
        */
        public function update(Request $request)
        {
            $id = $request -> edit_id;
            $data = pedidos::find($id);
            $tipo_operacion = $request -> edit_tipo_pago;// tipo de pago e
            
            $obsanteriores = $data -> observaciones ;
            $data -> observaciones = $obsanteriores." ".$request -> edit_estado_entrega.": ".$request -> edit_observaciones;
            if($request -> edit_estado_entrega=="pagar") 
            {
                $total_anterior= $data -> total_pagado;//250
                $nuevo_total = (float)$total_anterior + (float)$request -> edit_pago;//250-50= 200
                $data -> total_pagado = $nuevo_total;//200
                if((float)$data->total==(float)$nuevo_total)
                {
                     $data->estado_pago="PAGO_TOTAL";
                }
                else
                {
                   $data->estado_pago="PAGO_PARCIAL";
                }
                 $databitacora = new bitacorapagos;
                $databitacora->clienteid = $data->user_id;
                $databitacora->nombre = $data->user;
                $databitacora->pedido = $data->id;
                $databitacora->pago = $request -> edit_pago;
                $databitacora->tipo_pago =$tipo_operacion;
                $databitacora -> save();
            }
            else
            {
                $data -> estado_entrega = $request -> edit_estado_entrega;
            }
           
            $data -> save();


            return back()
                    ->with('finalizado','Datos modificados exitosamente.');
        }
        public function updateCerrar(Request $request)
        {
            $id = $request -> edit_id;
            $data = pedidos::find($id);
            $data -> checkin = 0;
            $data -> save();
            /*
             return back()
                    ->with('finalizado','Datos modificados exitosamente.');
                    */
                    return redirect()->action('pedidoController@index');



        }
 
        /*
        *   Delete record
        */
        public function delete(Request $request)
        {
            $id = $request -> id;
            $data = pedidos::find($id);
             $data -> activo = false;
            $response = $data -> update();
            if($response)
                echo "Dato eliminado exitosamente.";
            else
                echo "There was a problem. Please try again later.";
        }
}
