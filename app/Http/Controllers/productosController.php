<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\productos;
use App\User;
use DB;
use Auth;

class productosController extends Controller
{
    //
    public function index()
    {

         $data = productos::orderBy('id','DESC')
         ->get();
         return view('productos.index')->with('data',$data);
    }

   	
   public function add(Request $request)
        {

        	/*
        	 'name', 'email', 'password','nit','telefono','departamento','municipio','direccion', 'observaciones','tipo_usuario','tipo_entrega','activo', 'created_at', 'updated_at'
        	*/
            $data = new productos;
            $cod = $request -> codigo;
            $existcod = productos::where('codigo','=',$cod)->first();
            if($existcod)
            {
            		return back()
                    ->withErrors('Error','Codigo ya existe');	
            }
            else
            {
            	$data -> codigo = $request -> codigo;
	            $data -> Nombre = $request -> Nombre;
	            $data -> user_create_id = Auth::user()->id;
	            $data -> user =  Auth::user()->email;
	            $data -> save();
	            return back()
                    ->with('success','Producto ingresado exitosamente con codigo de barras No.'.$data->codigo);
	
            }
            
        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){

            $id = $request->id;
         	$info = productos::find($id);
                //echo json_decode($info);
                return response()->json($info);
        	}
        }

        public function update(Request $request)
        {
            $id = $request -> edit_id;
            $data = productos::find($id);
            if($request->edit_pedido==null)
            {

            }
            else
            {
            	$data -> pedido = $request -> edit_pedido;
            	
            }
            $codigo = $request -> edit_codigo;
            if((int)$codigo==(int)$data->codigo)
            {
            	$data -> activo = 0;
            	$data -> save();
            	return back()
                    ->with('finalizado','Datos modificados exitosamente.');
            }
            else
            {
            		//$data -> save();
            	return back()
                    ->withErrors('Error','Codigo de barras no coincide con producto.');	
            }
            
            
        }



}