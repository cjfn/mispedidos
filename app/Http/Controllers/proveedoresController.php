<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\proveedores;

class proveedoresController extends Controller
{
    //
	     public function index()
    {
         $data = proveedores::where('activo','=', '1')->get();
            return view('proveedores.index')->with('data',$data);
    }

   public function add(Request $request)
        {
            $data = new proveedores;
            $data -> nombre = $request -> nombre;
            $data -> direccion = $request -> direccion;
            $data -> telefono = $request -> telefono;
            $data -> correo = $request -> correo;
            $data -> nit = $request -> nit;
            $data -> contacto = $request -> contacto;
            $data -> formula = $request -> formula;
            $data -> save();
            return back()
                    ->with('success','Sucursal agregado exitosamente.');
        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                $info = proveedores::find($id);
                //echo json_decode($info);
                return response()->json($info);
            }
        }

        public function getproveedores(Request $request)
        {
            if($request->ajax()){
                
                $info = proveedores::all();
                //echo json_decode($info);
                return response()->json($info);
            }
        }

         /*
        *   Update data
        */
        public function update(Request $request)
        {
            $id = $request -> edit_id;
            $data = proveedores::find($id);
           $data -> nombre = $request -> edit_nombre;
            $data -> direccion = $request -> edit_direccion;
            $data -> telefono = $request -> edit_telefono;
            $data -> correo = $request -> edit_correo;
            $data -> nit = $request -> edit_nit;
            $data -> contacto = $request -> edit_contacto;
            $data -> formula = $request -> edit_formula;
            $data -> save();
            return back()
                    ->with('finalizado','Datos modificados exitosamente.');
        }
 
        /*
        *   Delete record
        */
        public function delete(Request $request)
        {
            $id = $request -> id;
            $data = proveedores::find($id);
             $data -> activo = false;
            $response = $data -> update();
            if($response)
                echo "Dato eliminado exitosamente.";
            else
                echo "There was a problem. Please try again later.";
        }
}
