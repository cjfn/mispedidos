<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\pedidos;
use App\models\detallespedidos;
use App\models\proveedores;
use App\models\bitacorapagos;
use App\User;
use DB;
use Auth;


class detallepedidosController extends Controller
{
    //
     public function index(Request $request)
    {
         $datacount = detallespedidos::where('pedido_id','=', $request->id)->count();
         $data = detallespedidos::where('pedido_id','=', $request->id)
         ->where('activo','=',1)
         ->get();
         $datadetallesp = pedidos::find($request->id);
         $user = User::find($request->user_id);
         $datadescuento = proveedores::find($datadetallesp->proveedor_id);
         $pedido_id = $request->id;
         
         $bitacorapagos = bitacorapagos::where('pedido','=',$request->id)
         ->get();
         
         /*
          $bitacorapagos = bitacorapagos::where('pedido','=',$request->id)->get();
          */
         
            return view('detallespedidos.index',  ['data'=>$data, 'user' => $user, 'pedido_id'=>$pedido_id,'datacount'=>$datacount, 'datadetallesp'=>$datadetallesp, 'datadescuento'=>$datadescuento,'bitacorapagos'=>$bitacorapagos]);

    }

   public function add(Request $request)
        {
            $data = new detallespedidos;
            $data -> user_id = Request()->user_id;
            $data -> codigo = $request -> codigo;
            $data -> pedido_id = $request -> pedido_id;
            $data -> nombre = $request -> nombre;
            $data -> talla = $request -> talla;
            $data -> color = $request -> color;
            $data -> cantidad = $request -> cantidad;
            $data -> user_create =  Auth::user()->email;
            $data -> user_create_id =  Auth::user()->id;
            $data -> precio_costo = $request -> precio_costo;
            $data -> precio_venta = $request -> precio_venta;
            $data -> catalogo = $request -> catalogo ;
            $data -> no_pagina = $request -> no_pagina ;
            $data -> save();

            $datadetallesp = pedidos::find($request->pedido_id);//busca ultimo pedido 81
            $ultimacantidad = $datadetallesp-> total_productos;//3
            $nuevototal = $ultimacantidad + $request -> cantidad;//3+2
            $datadetallesp -> total_productos = $nuevototal;//nuevo total de pedido
            $ultimototalprecio = (double)($datadetallesp -> total);//ultimo total de pedido
            $nuevototalprecio = (double)($request -> precio_venta);//0+75*1=75
            //$preciodescuento = $descuento*$request -> precio_venta;//0.1*75= 7.5
            //$nuevototalprecio = $nuevototalprecio-$preciodescuento;//75-7.5=67.5
            $datadetallesp -> total = ($nuevototalprecio)+($ultimototalprecio);
            $datadetallesp -> save();
            if($datadetallesp)
                  return back()
                    ->with('warning','Producto agregado a pedido No. '.$data->pedido_id.' exitosamente.');
            else
                 return back()
                    ->with('danger','Error en agregar producto a pedido. '.$data->pedido_id);
        }

        /*
         * View data
         */
        public function view(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                $info = detallespedidos::find($id);
                //echo json_decode($info);
                return response()->json($info);
            }
        }

        public function getproveedores(Request $request)
        {
            if($request->ajax()){
                
                $info = pedidos::all();
                //echo json_decode($info);
                return response()->json($info);
            }
        }

         /*
        *   Update data
        */
        public function update(Request $request)
        {
            //
            $id = $request -> edit_id;
            $detalle_pedido = $request -> edit_id_confirma;
            $data = detallespedidos::find($detalle_pedido);
            $data -> precio_costo = $request -> confirma_precio_costo;
            $data -> precio_venta = $request -> confirma_precio_venta;
            $datadetallesp = pedidos::find($data -> pedido_id);//busca ultimo pedido 81
            $nuevototal=$datadetallesp -> total_productos;//nuevo total de pedido
            $ultimototalprecio = (double)($datadetallesp -> total);//ultimo total de pedido
            $nuevototalprecio = (double)($request -> confirma_precio_venta);//0+75*1=75
            //$preciodescuento = $descuento*$request -> precio_venta;//0.1*75= 7.5
            //$nuevototalprecio = $nuevototalprecio-$preciodescuento;//75-7.5=67.5
            $datadetallesp -> total = ($nuevototalprecio)+($ultimototalprecio);
            $datadetallesp -> save();
            
            $data -> save();
            return back()
                    ->with('finalizado','Precios confirmados exitosamente exitosamente.');
          
        }
 
        /*
        *   Delete record
        */

            /*
        public function delete(Request $request)
        {
            
            $id = $request -> id;
            $data = detallespedidos::find($id);
            $cantidad_detalle = $data->cantidad;
            $total_detalle = $data -> precio_venta;
            $pedido = $data -> pedido_id;
            $data -> activo = false;
            $response = $data -> update();
            
            $id = $request -> id;
            $data = sucursal::find($id);
             $data -> activo = false;
            $response = $data -> update();
            if($response)
                echo "Dato eliminado exitosamente.";
            else
                echo "There was a problem. Please try again later.";

            
            $pedido = pedidos::find($pedido);
            $total = $pedido -> total;
            $cantidad = $pedido -> total_productos;
            $nuevo_total = $total - $total_detalle;
            $nueva_cantidad = $cantidad - $cantidad_detalle;
            $pedido->save();

            if($response)
                echo "Dato eliminado exitosamente.";
            else
                echo "There was a problem. Please try again later.";
               
        }
                      */

        public function Delete(Request $request)
        {
              if($request->ajax()){
                //echo json_decode($info);
                //$data = detallespedidos::find($id);
                $data =detallespedidos::where('id','=', $request->id)
                ->get();
                $data -> activo = 0;
                $response = $data -> update();
                return response()->json($info);
            }
            
        }

        public function destroy($id)
    {
        // delete
        $info = detallespedidos::find($id);
        $info->delete();

        // redirect
        return response()->json($info);
    }
}
