<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\pedidos;
use App\models\detallespedidos;
use App\models\proveedores;
use App\User;

class detalleapiController extends Controller
{
    //

    //

    
         /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $data = detallespedidos::all()->toArray();

            return response()->json($data);

            /*
            DB::table("Users as u")
          ->join ("departamento as d","u.id_departamento","=","d.id")
          ->join ("municipio as m","u.id_municipio","=","m.id")
          ->where('u.tipo_usuario','=', $query)
          ->where("activo","=", 1)
          ->select("u.id","u.name","u.email","u.nit","u.observaciones","u.telefono","u.tipo_entrega","u.tipo_usuario","u.updated_at","u.id_departamento","u.id_municipio","d.nombre_departamento","m.nombre_municipio","u.direccion","u.descuento", "u.created_at", "u.updated_at")
          
            ->orderBy('u.name','ASC')
          ->get();
          */
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $data = new detalle_factura;
            $data -> factura = $request -> factura;
            $data -> codigo_producto = $request ->  codigo_producto;
            $data -> cantidad = $request -> cantidad;
            $data -> subtotal = $request -> subtotal;
            $data -> fecha_vencimiento = $request -> fecha_vencimiento;
            $data -> usuario = $request -> usuario ;
            $data -> idunico = $request -> idunico ;
            $data -> catalogo = $request -> catalogo ;
            $data -> no_pagina = $request -> no_pagina ;
            
             
            $data -> save(); 



            return response()->json(['status'=>true, 'Producto agregado a factura'],200);
        }
        catch(Exception $e)
        {

            return response()->json(['status'=>false, 'error'],400);
        }

    }

     public function store_detalle(Request $request)
    {
        //

       
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          try{
      
       		$info=detallespedidos::find($id)->toArray();
            /*
            $info=DB::table("detallespedidos as d")
                ->where("m.pedido_id","=",$id)
              ->get();
              */
              
            return response()->json($info);
            

           
             
        }
        catch(Exception $e)
        {
            return response()->json(['status'=>false, 'error'],400);    
        }

                
            
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   

    public function update(Request $request, $id)
    {
       
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        

        $data = detallespedidos::find($id);
        $cantidad_detalle = $data->cantidad;
        $total_detalle = $data -> precio_venta;
        $pedido = $data -> pedido_id;
        $data -> activo = 0;
        $response = $data -> update();
        $pedido = pedidos::find($pedido);
        $totalanterior = $pedido->total;
        $nuevo_total = $totalanterior-$total_detalle;
        $pedido->total = $nuevo_total;
        $pedido->update();

    }

}
