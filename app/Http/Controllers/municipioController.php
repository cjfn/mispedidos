<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\municipios;
use DB;

class municipioController extends Controller
{
    //

      public function getMunicipios(Request $request)
        {
            if($request->ajax()){
                $id = $request->id;
                //$info = municipios::find($id);
                //echo json_decode($info);
                $info = DB::table("municipios as m")
                ->where("m.departamento","=",$id)
            ->select("m.id","m.nombre_municipio","u.direccion")
              ->get();
                return response()->json($info);
            }
        }
         /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $data = municipios::all()->toArray();

            return response()->json($data);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

     public function store_detalle(Request $request)
    {
        //

       
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          try{
      
       		$info=municipios::find($id);
            return response()->json($info);

           
             
        }
        catch(Exception $e)
        {
            return response()->json(['status'=>false, 'error'],400);    
        }

                
            
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   

    public function update(Request $request, $id)
    {
       
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
