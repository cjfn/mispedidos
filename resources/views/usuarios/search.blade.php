{!! Form::open(array('url'=>'usuarios','method'=>'GET','autocomplete'=>'on','role'=>'search')) !!}
<div class="form-group">
<br/>
  <div class="input-group">
    
    <input type="hidden" id="searchText" class="form-control" name="searchText" placeholder="Buscar usuarios por nombre..." value="{{$searchText}}">

     <select  name="tipo_usuario" title="seleccione tipo de usuario" required="true" class="form-control" onchange="SelectChanged();">
     				 <option value="">Seleccione tipo de usuario ... </option>  
                    <option value="cliente">Cliente/vendedor </option>
                    <option value="encargado">Encargado </option>
                    <option value="admin">Administrador </option>              
    </select>

    <span class="input-group-btn"> <div class="" aria-hidden="true"></div>

    <button type="submit" class="btn btn-info" id="btnsearch"><div class="fa fa-search"></div>Buscar</button>
    </span>
  </div>
</div>
<script type='text/javascript'>

function SelectChanged()  {  
   		alert(event.target.value);
   		var search = event.target.value;
   		document.getElementById("searchText").value= search;
   		document.getElementById("btnsearch").click();
 }

</script>

{{Form::close()}}