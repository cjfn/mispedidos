
@extends('layouts.appprincipal')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">
    <div class="row">
      <div class="col-md-2"></div>
          <div class="col-md-10">
            <div class="panel panel-default">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong>Clientes y usuarios</strong></div>
              </div>
            <div class="panel-body">
            <h2>Administra Clientes o vendedores y sus usuarios de acceso para que puedan hacer pedidos de productos a tus proveedores</h2>
            @include('usuarios.search')
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
                </div>
              @endif
    <div class="table-responsive">
    <table class="table table-bordered" id="MyTable">
    <center>
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal"><span class="fa fa-plus"></span>Agregar</button>
    </center>
      <thead>
        <tr>
          <th>Opciones</th>
          <th><strong>nombre</strong></th>
          <th>tipo de usuario</th>
          <th>descuento</th>
          <th>usuario</th>
          <th>telefono</th>
          <th>departamento</th>
          <th>municipio</th>
          <th>direccion</th>
          <th>fecha de creacion</th>

          
        </tr>
      </thead>
      <tbody>
      @foreach($data as $x)
        <tr>
           <td>
              <button title="ver mas detalles e informacion" class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')"><span class="fa fa-eye"></span>Ver</button>
              <button class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}')"><span class="fa fa-pencil-square"></span>Editar</button>
              <button class="btn btn-danger" onclick="fun_delete('{{$x -> id}}')"><span class="fa fa-times-circle"></span>Eliminar</button>
          </td>
          <td>{{$x -> name}}</td>
          <td>{{$x -> tipo_usuario}}</td>
          <td title="porcentaje $ de descuento y ultima modificacion">
            <center>
            <strong>{{$x -> descuento}}</strong><br/>{{$x -> updated_at}}
          </center>
          </td>
          <td>{{$x -> email}}</td>
          <td>{{$x -> telefono}}</td>
          <td>{{$x -> nombre_departamento}}</td>
          <td>{{$x -> nombre_municipio}}</td>
          <td>{{$x -> direccion}}</td>

          <td>{{$x -> created_at}}</td>
        </tr>
       @endforeach
      </tbody>
    </table>
  </div>
    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('usuarios/view')}}">
    <input type="hidden" name="changemun_url" id="changemun_url" value="{{url('municipios/view')}}">
    
     @if(Auth::user()->tipo_usuario=='admin')
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('usuarios/delete')}}">
    @else
    @endif
    <!-- Add Modal start -->
    <div class="modal fade" id="addModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agregar nuevo Usuario o cliente</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('usuarios') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Nombre:</label>
                  <input type="text" class="form-control" id="name" name="name" required="true">
                </div>
               

                <div class="form-group">
                  <label for="last_name">Tipo de usuario:</label> 
                    <select id="tipo_usuario" onchange="newtipouser()" onclick="" name="tipo_usuario" title="seleccione tipo de usuario" required="true"  class="form-control">
                      <option value="">Seleccione tipo de usuario ... </option>  
                      <option value="cliente">Cliente/vendedor </option>
                      <option value="encargado">Encargado </option>
                      <option value="admin">Administrador </option>
                   </select>
                </div>            

                    <div class="form-group">
                  <label for="last_name">Telefono:</label>
                  <input type="number" class="form-control" id="telefono" name="telefono" required="true">
                </div>
               <div class="form-group">
                  <label for="laboratorio_id">Departamento:</label>
                 <select id="departamento" onchange="changedep()" name="departamento" title="seleccione departamento" required="true" class="form-control">
                  <option value="">Seleccione una opcion ...</option>
                      @foreach($departamentos as $d)
                      <option value="{{ $d -> id}}">{{ $d -> nombre_departamento}} </option>
                  @endforeach
                   </select>
                </div>
                <div class="form-group">
                  <label for="laboratorio_id">Municipio:</label>
                 <select id="municipio"  name="municipio" title="seleccione municipio" required="true" class="form-control">

                  <option value="">Seleccione una opcion ...</option>
                      @foreach($municipios as $d)
                  
                      <option value="{{ $d -> id}}"> {{ $d -> nombre_municipio}} </option>
                  
                  @endforeach
                   </select>
                </div>
               
                  <input type="hidden" class="form-control" value="0" id="registro"  name="registro" required="true">
                
                <div class="form-group">
                  <label for="last_name">Direccion:</label>
                  <input type="text" class="form-control" id="direccion" name="direccion" required="true">
                </div>
                 <div class="form-group">
                  <label for="last_name">Tipo Entrega:</label>
                 
                  <select id="tipo_entrega" onchange="" name="tipo_entrega" title="seleccione tipo de entrega" required="true" class="form-control">
                    <option value="N/A" title="si cargo no es cliente o vendedor">NO APLICA</option>
                    <option value="PERSONAL">PERSONAL</option>
                    <option value="PAGO_CONTRA_ENTREGA">PAGO CONTRA ENTREGA</option>
                    <option value="EL_CORREO">EL CORREO</option>
                    <option value="CARGO_EXPRESO">CARGO_EXPRESO</option>
                    <option value="LITEGUA">LITEGUA</option>
                    <option value="OTRO" title="Especifique en observaciones">OTRO</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="last_name">Descuento:</label>
                  <input type="number" title="ingrese de 0 a 100" class="form-control" id="descuento" name="descuento" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Observaciones:</label>
                  <input type="text" class="form-control" id="observaciones" name="observaciones" required="true">
                </div>
               
              </div>
              
              <button type="submit" class="btn btn-default">Agregar</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
    <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalles</h4>
          </div>
          <div class="modal-body">
            <p><b>Nombre : </b><span id="view_name" class="text-success"></span></p>
            <p><b>Correo : </b><span id="view_email" class="text-success"></span></p>
            <p><b>Telefono : </b><span id="view_telefono" class="text-success"></span></p>
            <p><b>Tipo de Usuario : </b><span id="view_tipo_usuario" class="text-success"></span></p>
            <p><b>Tipo de Entrega : </b><span id="view_tipo_entrega" class="text-success"></span></p>
            <p><b>Descuento : </b><span id="view_descuento" class="text-success"></span></p>
            <p><b>Modificado el : </b><span id="view_updated" class="text-success"></span></p>
            <p><b>Nit : </b><span id="view_nit" class="text-success"></span></p>
            <p><b>Departamento : </b><span id="view_departamento" class="text-success"></span></p>
            <p><b>Municipio : </b><span id="view_municipio" class="text-success"></span></p>
            <p><b>Direccion : </b><span id="view_direccion" class="text-success"></span></p>
             <p><b>Observaciones : </b><span id="view_observaciones" class="text-success"></span></p>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
 
    <!-- Edit Modal start -->
    <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Editar</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('usuarios/update') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="edit_nombre">Nombre:</label>
                  <input type="text" class="form-control" id="edit_nombre" name="edit_nombre">
                </div>
                <div class="form-group">
                  <label for="edit_descripcion">Direccion :</label>
                  <input type="text" class="form-control" id="edit_direccion" name="edit_direccion">
                </div>
                <div class="form-group">
                  <label for="edit_telefono">telefono :</label>
                  <input type="text" class="form-control" id="edit_telefono" name="edit_telefono">
                </div>
                <div class="form-group">
                  <label for="edit_correo">correo :</label>
                  <input type="text" class="form-control" id="edit_correo" name="edit_correo">
                </div>
                 <div class="form-group">
                  <label for="edit_correo">Cambiar Tipo de usuario :</label>
                  <input type="text" readonly="readonly" name="thistusuario" id="thistusuario" class="form-control"> 
                  <input type="hidden" readonly="readonly" class="form-control" id="edit_tipo_usuario" name="edit_tipo_usuario">

                    <select id="e_tipo_usuario" onchange="newtipouser();" name="e_tipo_usuario" title="seleccione tipo de usuario" class="form-control">
                      <option value="">Seleccione tipo de usuario ... </option>  
                      <option value="cliente">Cliente/vendedor </option>
                      <option value="encargado">Encargado </option>
                      <option value="admin">Administrador </option>
                   </select>
                </div>
                 <div class="form-group">
                  <label for="edit_correo">Tipo de entrega :</label>
                  <input type="text" readonly="readonly" name="thistentrega" id="thistentrega" class="form-control">
                  <input type="hidden" class="form-control" id="edit_tipo_entrega" name="edit_tipo_entrega" class="form-control">

                   <select id="e_tipo_entrega"  name="e_tipo_entrega" title="seleccione tipo de entrega" required="true" class="form-control">
                    <option value="N/A" title="si cargo no es cliente o vendedor">NO APLICA</option>
                    <option value="PERSONAL">PERSONAL</option>
                    <option value="PAGO_CONTRA_ENTREGA">PAGO CONTRA ENTREGA</option>
                    <option value="EL_CORREO">EL CORREO</option>
                    <option value="CARGO_EXPRESO">CARGO_EXPRESO</option>
                    <option value="LITEGUA">LITEGUA</option>
                    <option value="OTRO" title="Especifique en observaciones">OTRO</option>
                  </select>
                </div>
                
                 <div class="form-group">
                  <input type="hidden" name="edit_departamento" id="edit_departamento">
                  <label for="">Departamento:</label>
                   <input type="text" readonly="readonly" name="thisdepartamento" id="thisdepartamento" class="form-control">
                 <select id="edit_depa" onchange="chandep()" name="edit_depa" title="seleccione nuevo departamento"  class="form-control">
                  <option value="">Seleccione una opcion ...</option>
                      @foreach($departamentos as $d)
                  
                      <option value="{{ $d -> id}}">{{ $d -> nombre_departamento}} </option>
                  
                  @endforeach
                   </select>
                </div>
                 <div class="form-group">
                  <label for="edit_correo">municio :</label>
                  <input type="text" readonly="readonly" name="thismunicipio" id="thismunicipio" class="form-control">
                  <input type="hidden" class="form-control" id="edit_municipio" name="edit_municipio">
                  <select id="edit_mun" onchange="changemun()" name="edit_mun" title="seleccione nuevo departamento"  class="form-control">
                  <option value="">Seleccione una opcion ...</option>
                      @foreach($municipios as $d)
                  
                      <option value="{{ $d -> id}}">{{ $d -> nombre_municipio}} </option>
                  
                  @endforeach
                   </select>
                </div>
                <div class="form-group">
                  <label for="last_name">Descuento:</label>
                  <input type="number" title="ingrese de 0 a 100" class="form-control" id="edit_descuento" name="edit_descuento" required="true">
                </div>
                 <div class="form-group">
                  <label for="last_name">Observaciones:</label>
                  <input type="text" class="form-control" id="edit_observaciones" name="edit_observaciones" >
                </div>


               
              </div>


              
              <button type="submit" class="btn btn-default">Modificar</button>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
          
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->
 
  </div>
  </div>
  </div>


                <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
                <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

                <script language="javascript" type="text/javascript">
                                    
                    $(document).ready(function () {
                      $('#MyTable').DataTable();

                       $('#addModal').on('shown.bs.modal', function() {
                        $('#name').focus();
                      });
                  } );


    function newtipouser()  {  
          var search = document.getElementById("e_tipo_usuario").value;
          alert("cambiar tipo de usuario"+search);
          document.getElementById("edit_tipo_usuario").value= search;
          //document.getElementById("edit_tipo_usuario").style.visibility = "visible";
          //document.getElementById("btnsearch").click();
     }

     function changemun(){
          //alert(event.target.value);
          var id = document.getElementById("edit_mun").value;
          alert("Cambiar nuevo municipio"+id);
           document.getElementById("edit_municipio").value= id;
      };
      
      function chandep(){
          var id = document.getElementById("edit_depa").value;
          alert("Cambiar nuevo departamento"+id);
           document.getElementById("edit_departamento").value= id;
      };


      function chantentrega(){
          var id = document.getElementById("e_tipo_entrega").selectedIndex;
          alert("Cambiar tipo de entrega"+id);
           document.getElementById("edit_tipo_entrega").value= id;
      };

     
 
    function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          //alert(id);
          $("#view_name").text(result.name);
          $("#view_direccion").text(result.direccion);
          $("#view_telefono").text(result.telefono);
          $("#view_email").text(result.email);
          $("#view_departamento").text(result.nombre_departamento);
          $("#view_municipio").text(result.nombre_municipio);
          $("#view_tipo_usuario").text(result.tipo_usuario);
          $("#view_tipo_entrega").text(result.tipo_entrega);
          $("#view_observaciones").text(result.observaciones);
          $("#view_descuento").text(result.descuento);
          $("#view_updated").text(result.updated_at);
          

        }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          console.log(result);
          $("#edit_id").val(result.id);
          $("#edit").val(result.nombre);
          $("#edit_direccion").val(result.direccion);
          $("#edit_telefono").val(result.telefono);
          $("#edit_correo").val(result.email);
          $("#edit_nombre").val(result.name);
          $("#edit_departamento").val(result.id_departamento);
          $("#edit_municipio").val(result.id_municipio);
          $("#thisdepartamento").val(result.nombre_municipio);
          $("#thismunicipio").val(result.nombre_departamento);
          $("#edit_tipo_entrega").val(result.tipo_entrega);
          $("#edit_tipo_usuario").val(result.tipo_usuario);
          $("#thistentrega").val(result.tipo_entrega);
          $("#thistusuario").val(result.tipo_usuario);
          $("#edit_observaciones").val(result.observaciones);
          $("#edit_descuento").val(result.descuento);
        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Esta seguro que desea eliminar usuario No."+id+"??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }
 </script>       
          </div>
          <!-- /.col-lg-12 -->
      </div>
</div>

@endsection

