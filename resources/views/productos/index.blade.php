
@extends('layouts.appprincipal')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">
    <div class="row">
      <div class="col-md-2"></div>
          <div class="col-md-10">
            <div class="panel panel-default">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong>TUS PRODUCTOS DE INGRESO</strong></div>
              </div>
            <div class="panel-body">
            <h2>Administra los productos de ingreso para distribucion a clientes o vendedores</h2>
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">Ã—</button>
                  <strong>{{ $message }}</strong>
                </div>
                @endif
              @if($message = Session::get('errors'))
                <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">Ã—</button>
                  <strong>Error! verifique los datos ingresados o intente de nuevo</strong>
                </div>

              @endif
    <div class="table-responsive">
    <table class="table table-bordered" id="MyTable">
    <center>
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal"><span class="fa fa-plus"></span>Agregar</button>
    </center>
      <thead>
        <tr>
          <th>Opciones</th>
          <th><strong>Codigo de barras</strong></th>
          <th>Nombre</th>
          <th>Fecha ingreso </th>
          <th>En bodega</th>
        </tr>
      </thead>
      <tbody>
      @foreach($data as $x)
        <tr>
            <td>
              <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')"><span class="fa fa-eye"></span>Detalles</button>
              <button class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}')"><span class="fa fa-pencil-square"></span>Entregar</button>
              <button class="btn btn-danger" title="Entregar a cliente"><span class="fa fa-times-circle" onclick="fun_edit('{{$x -> id}}')"></span>Devolucion</button>
          </td>
          <td><strong><center>{{$x -> codigo}}</center></strong></td>
          <td>{{$x -> nombre}}</td>
          <td>{{$x -> created_at}}</td>
          @if($x->activo==1)
          <td style="color:orange">En Bodega</td>
          @elseif($x->activo==0)
          <td style="color:green">Entregado</td>
           @elseif($x->activo==3)
          <td style="color:red">Devuelto</td>
          @endif

        </tr>
       @endforeach
      </tbody>
    </table>
  </div>
    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('productos/view')}}">
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('productos/delete')}}">
    <!-- Add Modal start -->
    <div class="modal fade" id="addModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agregar nuevo Producto</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('productos') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Codigo de barras:</label>
                  <input type="text" class="form-control" id="codigo" name="codigo" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Nombre:</label>
                  <input type="text" class="form-control" id="nombre" name="nombre">
                </div>
                
              </div>
              
              <button type="submit" class="btn btn-default">Agregar</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
    <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalles</h4>
          </div>
          <div class="modal-body">
            <p><b>Codigo de barras : </b><span id="view_codigo" class="text-success"></span></p>
            <p><b>Nombre : </b><span id="view_nombre" class="text-success"></span></p>
            <p><b>Fecha Ingreso : </b><span id="view_fecha" class="text-success"></span></p>
            <p><b>En bodega : </b><span id="view_activo" class="text-success"></span></p>
            <p><b>Creado por : </b><span id="view_usuario" class="text-success"></span></p>
             <p><b>Fecha Entrega : </b><span id="view_fecha_entrega" class="text-success"></span></p>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
 
    <!-- Edit Modal start -->
    <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Editar</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('productos/update') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="edit_nombre">Ingrese codigo de barras:</label>
                  <input type="text" class="form-control" id="edit_codigo" name="edit_codigo" required="true">
                </div>
                <div class="form-group">
                  <label for="edit_nombre">Pedido:</label>
                  <input type="number" class="form-control" id="edit_pedido" name="edit_pedido">
                </div>
                
              </div>
              
              <button type="submit" class="btn btn-default">Entregar a cliente</button>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
          
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->
 
  </div>
  </div>
  </div>

 <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

                 
                 <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

                <script language="javascript" type="text/javascript">
                                    
                    $(document).ready(function () {
                      $('#MyTable').DataTable();

                       $('#addModal').on('shown.bs.modal', function() {
                        $('#codigo').focus();
                      });
                  } );
                </script>



  <script type="text/javascript">
    function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      //alert(id);
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          console.log(result);
          $("#view_codigo").text(result.codigo);
          $("#view_nombre").text(result.nombre);
          $("#view_usuario").text(result.user);
          var en_bodega = result.activo;
          if(en_bodega==1)
          {
            $("#view_activo").text("EN BODEGA");            
          }
          else
          {
            $("#view_activo").text("ENTREGADO");

          }
          $("#view_fecha").text(result.created_at);
          $("#view_fecha_entrega").text(result.updated_at);
        }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      //alert(id);
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          console.log(result);
          $("#edit_id").val(result.id);

        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Esta seguro que desea eliminar??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }
 </script>       
          </div>
          <!-- /.col-lg-12 -->
      </div>
</div>

@endsection
