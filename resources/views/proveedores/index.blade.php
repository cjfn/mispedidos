
@extends('layouts.appprincipal')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">
    <div class="row">
      <div class="col-md-2"></div>
          <div class="col-md-10">
            <div class="panel panel-default">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong>TUS PROVEEDORES</strong></div>
              </div>
            <div class="panel-body">
            <h2>Administra proveedores de tus productos</h2>
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
                </div>
              @endif
    <div class="table-responsive">
    <table class="table table-bordered" id="MyTable">
    <center>
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal"><span class="fa fa-plus"></span>Agregar</button>
    </center>
      <thead>
        <tr>
          <th><strong>CODIGO</strong></th>
          <th>Nombre</th>
          <th>Direccion</th>
          <th>Telefono</th>
          <th>correo</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($data as $x)
        <tr>
          <td><strong><center>{{$x -> id}}</center></strong></td>
          <td>{{$x -> nombre}}</td>
          <td>{{$x -> direccion}}</td>
          <td>{{$x -> telefono}}</td>
          <td>{{$x -> correo}}</td>

          <td>
              <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')"><span class="fa fa-eye"></span>Ver</button>
              <button class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}')"><span class="fa fa-pencil-square"></span>Editar</button>
              <button class="btn btn-danger" title="Opcion desabilitada"><span class="fa fa-times-circle"></span>Eliminar</button>
          </td>
        </tr>
       @endforeach
      </tbody>
    </table>
  </div>
    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('proveedores/view')}}">
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('proveedores/delete')}}">
    <!-- Add Modal start -->
    <div class="modal fade" id="addModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agregar nuevo Proveedor</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('proveedores') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Nombre:</label>
                  <input type="text" class="form-control" id="nombre" name="nombre" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Direccion:</label>
                  <input type="text" class="form-control" id="direccion" name="direccion" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Telefono:</label>
                  <input type="number" class="form-control" id="telefono" name="telefono" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">correo:</label>
                  <input type="text" class="form-control" id="correo" name="correo" required="true">
                </div>
                 <div class="form-group">
                  <label for="last_name">nit:</label>
                  <input type="text" class="form-control" id="nit" name="nit" required="false">
                </div>
                 <div class="form-group">
                  <label for="last_name">contacto:</label>
                  <input type="text" class="form-control" id="contacto" name="contacto" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">formula:</label>
                  <input title="esta formula sera utlizada para calcular el precio final del producto ingresado" type="text" class="form-control" id="formula" name="formula" required="true">
                </div>
              </div>
              
              <button type="submit" class="btn btn-default">Agregar</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
    <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalles</h4>
          </div>
          <div class="modal-body">
            <p><b>Nombre : </b><span id="view_nombre" class="text-success"></span></p>
            <p><b>Direccion : </b><span id="view_direccion" class="text-success"></span></p>
            <p><b>Telefono : </b><span id="view_telefono" class="text-success"></span></p>
            <p><b>Encargado : </b><span id="view_correo" class="text-success"></span></p>
            <p><b>Nit : </b><span id="view_nit" class="text-success"></span></p>
            <p><b>Contacto : </b><span id="view_contacto" class="text-success"></span></p>
            <p><b>Formula : </b><span id="view_forumla" class="text-success"></span></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
 
    <!-- Edit Modal start -->
    <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Editar</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('proveedores/update') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="edit_nombre">Nombre:</label>
                  <input type="text" class="form-control" id="edit_nombre" name="edit_nombre">
                </div>
                <div class="form-group">
                  <label for="edit_descripcion">Direccion :</label>
                  <input type="text" class="form-control" id="edit_direccion" name="edit_direccion">
                </div>
                <div class="form-group">
                  <label for="edit_telefono">telefono :</label>
                  <input type="text" class="form-control" id="edit_telefono" name="edit_telefono">
                </div>
                <div class="form-group">
                  <label for="edit_correo">correo :</label>
                  <input type="text" class="form-control" id="edit_correo" name="edit_correo">
                </div>
                 <div class="form-group">
                  <label for="edit_correo">nit :</label>
                  <input type="text" class="form-control" id="edit_nit" name="edit_nit">
                </div>
                 <div class="form-group">
                  <label for="edit_correo">contacto :</label>
                  <input type="text" class="form-control" id="edit_contacto" name="edit_contacto">
                </div>
                <div class="form-group">
                  <label for="edit_correo">Cambiar Formula :</label>
                  <input type="text" class="form-control" id="edit_formula" name="edit_formula">
                </div>
               
              </div>
              
              <button type="submit" class="btn btn-default">Modificar</button>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
          
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->
 
  </div>
  </div>
  </div>

 <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

                 
                 <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

                <script language="javascript" type="text/javascript">
                                    
                    $(document).ready(function () {
                      $('#MyTable').DataTable();

                       $('#addModal').on('shown.bs.modal', function() {
                        $('#nombre').focus();
                      });
                  } );
                </script>



  <script type="text/javascript">
    function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#view_nombre").text(result.nombre);
          $("#view_direccion").text(result.direccion);
          $("#view_telefono").text(result.telefono);
          $("#view_correo").text(result.correo);
          $("#view_nit").text(result.nit);
          $("#view_contacto").text(result.contacto);
          $("#view_forumla").text(result.formula);
        }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#edit_id").val(result.id);
          $("#edit_nombre").val(result.nombre);
          $("#edit_direccion").val(result.direccion);
          $("#edit_telefono").val(result.telefono);
          $("#edit_correo").val(result.correo);
          $("#edit_nit").val(result.nit);
          $("#edit_contacto").val(result.contacto);
          $("#edit_formula").val(result.formula);
        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Esta seguro que desea eliminar??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }
 </script>       
          </div>
          <!-- /.col-lg-12 -->
      </div>
</div>

@endsection

