

@extends('layouts.app')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">
  <center>

 <span  tabindex="4" onclick="printDiv('areaImprimir')" onkeypress="printDiv('areaImprimir')" value="imprimir div" class="btn btn-danger btn-lg" ><span class="fa fa-print"></span>IMPRIMIR</span>
</center>
<div id="areaImprimir" >



          <CENTER>
          <h2> <STRONG> REPORTE DE PEDIDOS REALIZADOS A PROVEEDORES</STRONG></h2>
          <H5>FECHA: {{ $date }} - REPORTE DE PEDIDOS REALIZADOS A PROVEEDOR {{$proveedores['nombre']}} </H5>
            <H4>DEL {{substr($from, 0, 10)}} AL {{substr($to, 0, 10)}}</H4>


          
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="">No. Pedido.</th>
            <th class="">Total Q.</th>
            <th class="">Total Pagado.</th>
            <th class="">Descuento %</th>
            <th class="">Estado Entrega</th>
            <th class="">Creacion</th>
            <th class="">Cambio de estado</th>
            
            
      

          </tr>
        </thead>
        <tbody>

          <input type="hidden" value="{{$total = 0}}" name="">
          <input type="hidden" value="{{$totalpagado = 0}}" name="">
             
          
             @foreach($data as $key => $dat)
             <tr>
            <td class="">{{ $dat->id }}</td>
            <td> {{ $dat->total }}</td>
            <td> {{ $dat->total_pagado }}</td>
            <td> {{ $dat->porcentaje_descuento }}</td>
             <input type="hidden" value=" {{$total = $total + $dat->total}}" name="">
            <input type="hidden" value=" {{$totalpagado = $totalpagado + $dat->total_pagado}}" name="">
           
     
            <td> {{ $dat->estado_entrega }}</td>
             <td> {{ $dat->created_at }}</td>
             <td> {{ $dat->updated_at }}</td>
            </tr>
            @endforeach

          

        </tbody>
        <CENTER>
        <h3>
           Total<strong>Q.{{number_format($total, 2)}}</strong>
          Total pagado<strong>Q.{{number_format($totalpagado, 2)}}</strong>
            
        </h3>
        </CENTER>
       
      </table>
   </div>

  <script type="text/javascript">
    

      function printDiv(nombreDiv) {
     var contenido= document.getElementById(nombreDiv).innerHTML;
     var contenidoOriginal= document.body.innerHTML;

     document.body.innerHTML = contenido;

     window.print();

     document.body.innerHTML = contenidoOriginal;
}
  </script>

@endsection


