
@extends('layouts.app')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">
  <center>

 <span  tabindex="4" onclick="printDiv('areaImprimir')" onkeypress="printDiv('areaImprimir')" value="imprimir div" class="btn btn-danger btn-lg" ><span class="fa fa-print"></span>IMPRIMIR</span>
</center>
<div id="areaImprimir" >
  <input type="hidden" id="id"
        name="id" value={{ $total = 0 }}>  
        <center>     
       DISTRIBUIDORA DONAJI
       <br/>
          <script type="text/javascript">
           var d = new Date();
            var month = new Array();
            month[0]="1";
            month[1]="2";
            month[2]="3";
            month[3]="4";
            month[4]="5";
            month[5]="6";
            month[6]="7";
            month[7]="8";
            month[8]="9";
            month[9]="10";
            month[10]="11";
            month[11]="12";
          var mes = month[d.getMonth()];



           document.write(d.getDate()+"/"+mes+"/"+d.getFullYear()+"  "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds());
           </script>
         <br/>
        PEDIDO PEDIDO NO. {{Request()->id}}      <br/>
        {{$user->name}}<br/>

        </center>
          -------------------------------------------------------------------------------------------------        
        <br/>
        <table style="border: solid 1px #000000; ">      
                  <tr>
                   
                    <th style="border: solid 1px #000000; "><strong>Nombre producto</strong></th>
                    <th style="border: solid 1px #000000; "> Talla</th>
                    <th style="border: solid 1px #000000; "> Color</th>
                    <th style="border: solid 1px #000000; "> Cantidad</th>
                    <th style="border: solid 1px #000000; "> Descuento %</th>
                    <th style="border: solid 1px #000000; "> Venta Q.</th>
                    
                  </tr>

                </thead>
                <tbody>
                @foreach($data as $x)
                  <tr>
                    <td style="border: solid 1px #000000; ">{{$x -> codigo}} - {{$x -> nombre}}</td>
                    <td style="border: solid 1px #000000; ">{{$x -> talla}}</td>
                    <td style="border: solid 1px #000000; ">{{$x -> color}}</td>
                    <td style="border: solid 1px #000000; ">{{$x -> cantidad}}</td>
                    <td style="border: solid 1px #000000; ">{{$datadetallesp->porcentaje_descuento}}%</td>
                    <td style="border: solid 1px #000000; ">{{number_format($x -> precio_venta, 2)}}</td>
                    
                  </tr>
                 @endforeach
                </tbody>
              </table>
              @foreach($bitacorapagos as $b)
              @if($b->id!=null)
                <h2 style="color:red; text-align: right;"> Anticipo:
                  <strong > Q.{{$b->pago}}</strong>
                </h2>
              @else
              @endif
            @endforeach 
                   -------------------------------------------------------------------------------------------------   
        <br/>
              <h3 style="color:blue; text-align: right;" > Total:
              <strong >Q.{{number_format($datadetallesp->total, 2)}}</strong>
              </h3>
       
   </div>

  <script type="text/javascript">
    

      function printDiv(nombreDiv) {
     var contenido= document.getElementById(nombreDiv).innerHTML;
     var contenidoOriginal= document.body.innerHTML;

     document.body.innerHTML = contenido;

     window.print();

     document.body.innerHTML = contenidoOriginal;
}
  </script>

@endsection


