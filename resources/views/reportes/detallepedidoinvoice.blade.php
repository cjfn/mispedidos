
@extends('layouts.app')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">
  <center>

 <span  tabindex="4" onclick="printDiv('areaImprimir')" onkeypress="printDiv('areaImprimir')" value="imprimir div" class="btn btn-danger btn-lg" ><span class="fa fa-print"></span>IMPRIMIR</span>
</center>
<div id="areaImprimir" >
  <input type="hidden" id="id"
        name="id" value={{ $total = 0 }}>  
        <center>     
       DISTRIBUIDORA DONAJI
       <br/>
         
        {{$datadetallesp->created_at}}
         <br/>
        PEDIDO PEDIDO NO. {{Request()->id}}      <br/>
        {{$user->name}}<br/>

        </center>      
        <br/>
        <table class="table table-bordered">      
                  <tr>
                   
                    <th><strong>Nombre producto</strong></th>
                    <th> Talla</th>
                    <th> Color</th>
                    <th> Cantidad</th>
                    <th> Descuento %</th>
                    <th> Venta Q.</th>
                    
                  </tr>

                </thead>
                <tbody>
                @foreach($data as $x)
                  <tr>
                    <td>{{$x -> codigo}} - {{$x -> nombre}}</td>
                    <td>{{$x -> talla}}</td>
                    <td>{{$x -> color}}</td>
                    <td>{{$x -> cantidad}}</td>
                    <td>{{$datadetallesp->porcentaje_descuento}}%</td>
                    <td>{{number_format($x -> precio_venta, 2)}}</td>
                    
                  </tr>
                 @endforeach
                </tbody>
              </table>
             

  <input type="hidden" name="" value="{{$total_devolucion=0}}">
  <input type="hidden" name="" value="{{$total_anticipo=0}}">
  <input type="hidden" name="" value="{{$total_pago=0}}">

              @foreach($bitacorapagos as $b)

              @if($b->tipo_pago=="ANTICIPO")

              <input type="hidden" name="" value="{{$total_anticipo=$b->pago+$total_anticipo}}">

              @elseif($b->tipo_pago=="DEVOLUCION")
              <input type="hidden" name="" value="{{$total_devolucion=$b->pago+$total_devolucion}}">
               @elseif($b->tipo_pago=="PAGO"||$B->tipo_pago=="ANTICIPO")
              <input type="hidden" name="" value="{{$total_pago=$b->pago+$total_pago}}">
             @else
             @endif
             
              @endforeach
               <p style="color:red; text-align: right;"> Devolucion:
                  <strong>+ Q.{{$total_devolucion}}</strong>
                </p> 
                 <p style="color:orange; text-align: right;"> Anticipo:
                  <strong >+ Q.{{$total_anticipo}}</strong>
                </p> 
              <input type="hidden" name="" value="{{$tpagado=0}}">

                
              <p style="color:green; text-align: right;" > Total Pagado:
              <strong >+ Q.{{number_format($total_pago
              , 2)}}</strong>
              </p>
              <p style="color:blue; text-align: right;" > Total:
              <strong >Q.{{$tpagado = number_format($datadetallesp->total, 2)}}</strong>
              </p>
              @if($datadetallesp->total!=$datadetallesp->total_pagado)
              <p style="color:red; text-align: right;">Pendientes Pago
               <strong>Q.{{$datadetallesp->total-$datadetallesp->total_pagado}}</strong> 
              </p>
              @else
              @endif



       
   </div>

  <script type="text/javascript">
    

      function printDiv(nombreDiv) {
     var contenido= document.getElementById(nombreDiv).innerHTML;
     var contenidoOriginal= document.body.innerHTML;

     document.body.innerHTML = contenido;

     window.print();

     document.body.innerHTML = contenidoOriginal;
}
  </script>

@endsection


