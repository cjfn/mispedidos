
@extends('layouts.appprincipal')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">
    <div class="row">
      <div class="col-md-2">
      </div>
      <div class="col-md-10">       
        <div class="panel panel-primary">
            <div class="panel-heading">CREA REPORTES
            </div>
        <div class="panel-body">
    <h2>Crea reporte de tus ventas, productos y mucho mas</h2>
    @if ($message = Session::get('success'))

      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
    <div class="table-responsive">
    <table class="table table-bordered" id="MyTable">
    <center>
      <a href="{{ URL::previous() }}" class="btn btn-default"><span class="fa fa-mail-reply"></span>Atras</a>
  <h3> <strong>SELECCIONE</strong> su tipo de repote
    
    </center>
      <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Bienvenido</a></li>
    <li><a data-toggle="tab" href="#menu1">1. Reporte pedidos por cliente</a></li>
    <li><a data-toggle="tab" href="#menu2">2. Reporte de ventas por proveedor</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h3>Reportes</h3>
      <p>Seleccione el tipo de reporte a generar para vista de resultados</p>
    </div>
    <div id="menu1" class="tab-pane fade">
     <h3>Reporte de pedidos por fecha y cliente</h3>
        <form action="" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Fecha de inicio:</label>
                  <input type="date" tabindex="1" class="form-control" id="from1" name="from1" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Fecha de fin:</label>
                  <input type="date" tabindex="2" class="form-control" id="to1" name="to1" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">CLIENTE</label>
                 <select id="usuario"  name="usuario" title="seleccione usuario origen" required="true" tabindex="3" class="form-control">
                  <option value="TODOS">TODOS</option>
                      @foreach($user as $s)
                  
                      <option value="{{ $s -> id}}">Cod: {{ $s -> id}} - {{ $s -> name}} </option>
                  
                  @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="last_name">ESTADO DE ENTREGA</label>
                 <select id="entrega"  name="entrega" title="seleccione estado del pedido" required="true" tabindex="3" class="form-control">
                      <option value="TODOS">TODOS</option>
                      <option value="PENDIENTES_ENTREGA">PENDIENTES DE ENTREGA</option>
                      <option value="PENDIENTE_PEDIDO">PENDIENTES DE HACER PEDIDO</option>
                      <option value="PEDIDO_PROVEEDOR">PEDIDO A PROVEEDOR</option>
                      <option value="PEDIDO_ENTREGADO_CLIENTE">ENTREGADO A CLIENTE</option>
                  
                  </select>
                </div>
                <div class="form-group">

                  <label for="last_name">ESTADO DE PAGO</label>
                 <select id="pago"  name="pago" title="seleccione estado del pago pedido" required="true" tabindex="3" class="form-control">
                      <option value="TODOS">TODOS</option>
                      <option value="PAGO_PARCIAL" title="CLIENTE CON DEUDA">PAGO PARCIAL</option>
                      <option value="PAGO_TOTAL">PAGO TOTAL</option>
                      <option value="PEDIDO_PROVEEDOR">PEDIDO A PROVEEDOR</option>
                      <option value="PEDIDO_ENTREGADO_CLIENTE">ENTREGADO A CLIENTE</option>
                  
                  </select>
                </div>
              </div>
              <center>
              <span class="btn btn-success btn-lg" tabindex="3" onclick="reportepedidos()" onkeypress="reportepedidos()"><span class="fa fa-file-pdf-o"></span>Ver reporte por  fechas y cliente/vendedor</span>
              </center>
      </form>
      </div>
    <div id="menu2" class="tab-pane fade">
      <h3>Reporte de pedidos por fecha y cliente</h3>
        <form action="" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Fecha de inicio:</label>
                  <input type="date" tabindex="1" class="form-control" id="from2" name="from2" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Fecha de fin:</label>
                  <input type="date" tabindex="2" class="form-control" id="to2" name="to2" required="true">
                </div>
                <div class="form-group">

                  <label for="last_name">Proveedor:</label>
                 <select id="proveedor_id"  name="proveedor_id" title="seleccione proveedor de  origen" required="true" tabindex="3" class="form-control">
                      @foreach($proveedores as $s)
                  
                      <option value="{{ $s -> id}}">Cod: {{ $s -> id}} - {{ $s -> nombre}} </option>
                  
                  @endforeach
                  </select>
                </div>
               
              </div>
              <center>
              <span class="btn btn-success btn-lg" tabindex="3" onclick="reportepedidosProveedor()" onkeypress="reportepedidosProveedor()"><span class="fa fa-file-pdf-o"></span>Ver reporte por  fechas y cliente/vendedor</span>
              </center>
      </form>
    </div>
   
  </div>



    <ul class="nav nav-tabs">
     @if(Auth::user()->nombre_tipo_usuario=='admin')
           
   <li class="active"><a data-toggle="tab" href="#home">Ventas</a></li>
      <li><a data-toggle="tab" href="#menu1">Ingresos</a></li>
      <li><a data-toggle="tab" href="#menu2">Ajustes/descargas</a></li>
                    @endif

      
    </ul>

    <div class="tab-content">
  
      <div id="home" class="tab-pane fade in active">
        @if(Auth::user()->nombre_tipo_usuario=='admin')
  
      <div id="menu1" class="tab-pane fade">
        <h3>Reporte de ingresos por fecha</h3>
        <form action="" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Fecha de inicio:</label>
                  <input type="date" tabindex="1" class="form-control" id="from2" name="from2" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Fecha de fin:</label>
                  <input type="date" tabindex="2" class="form-control" id="to2" name="to2" required="true">
                </div>
                 <div class="form-group">

                  <label for="last_name">Sucursal</label>
                 <select id="sucursal2"  name="sucursal2" title="seleccione sucursal de bodega" required="true" tabindex="3" class="form-control">
                  <option value="TODOS">TODOS</option>
                      @foreach($proveedores as $s)
                  
                      <option value="{{ $s -> id}}">Cod: {{ $s -> id}} - {{ $s -> nombre}} </option>
                  
                  @endforeach
                  </select>
                </div>
               
              </div>
              <center>
              <span class="btn btn-success btn-lg" tabindex="3" onclick="reportein()" onkeypress="reportein()"><span class="fa fa-file-pdf-o"></span>Ver reporte por  fechas</span>
              </center>
             

      </form>
     
    </div>
      
      <div id="menu2" class="tab-pane fade">
        <h3>Reporte de ajustes por fecha</h3>
        <form action="" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                 
                <div class="form-group">
                  <label for="first_name">Fecha de inicio:</label>
                  <input type="date" tabindex="1" class="form-control" id="from3" name="from3" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Fecha de fin:</label>
                  <input type="date" tabindex="2" class="form-control" id="to3" name="to3" required="true">
                </div>
                 <div class="form-group">

                  <label for="last_name">Proveedor:</label>
                 <select id="sucursal3"  name="sucursal3" title="seleccione sucursal de bodega" required="true" tabindex="3" class="form-control">
                      @foreach($proveedores as $s)
                  
                      <option value="{{ $s -> id}}">Cod: {{ $s -> id}} - {{ $s -> nombre}} </option>
                  
                  @endforeach
                  </select>
                </div>
               
              </div>
              <center>
              <span class="btn btn-success btn-lg" tabindex="3" onclick="reporteajustes()" onkeypress="reporteajustes()"><span class="fa fa-file-pdf-o"></span>Ver reporte por  fechas</span>
              </center>
      </form>
   @endif
      </div>


       <div id="menu3" class="tab-pane fade">
        <h3>Reporte de fechas de vencimiento</h3>
        <form action="" method="post">
              {{ csrf_field() }}
              <div class="form-group">

                <div class="form-group">
                  <label for="first_name">Laboratorio:</label>
                  <span class="form-control">
                  <select class="itemName form-control" style="width:700px;" tabindex="1" id="laboratorio_id" name="laboratorio_id" required="true" ></select>
                </span>
                </div>
                <div class="form-group">
                  <label for="first_name">Fecha de inicio:</label>
                  <input type="date" tabindex="2" class="form-control" id="from4" name="from4" required="true">
                </div>
                <div class="form-group">
                  <label for="last_name">Fecha de fin:</label>
                  <input type="date" tabindex="3" class="form-control" id="to4" name="to4" required="true">
                </div>
                 <div class="form-group">
                 <select id="sucursal4"  name="sucursal4" title="seleccione sucursal de bodega" required="true" tabindex="3" class="form-control">
                      @foreach($proveedores as $s)
                  
                      <option value="{{ $s -> id}}">Cod: {{ $s -> id}} - {{ $s -> nombre}} </option>
                  
                  @endforeach
                  </select>
                </div>
               
              </div>
              <center>
              <span class="btn btn-success btn-lg" tabindex="3" onclick="reportevencimiento()" onkeypress="reportevencimiento()"><span class="fa fa-file-pdf-o"></span>Ver reporte por  fechas</span>
              </center>
      </form>
      </div>
    </div>
    
  </div>
    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('reportes/view')}}">
    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('reportes/delete')}}">
    
  </div>
  </div>
  </div>
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">



function printDiv(nombreDiv) {
     var contenido= document.getElementById(nombreDiv).innerHTML;
     var contenidoOriginal= document.body.innerHTML;

     document.body.innerHTML = contenido;

     window.print();

     document.body.innerHTML = contenidoOriginal;
}
//SELECT DE ALUMNO
      $('.itemName').select2({
        placeholder: 'Seleccione un Laboratorio',
        ajax: {
          url: '{{ url('/select2-autocomplete-ajax') }}',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                    return {

                        text: item.nombre+' '+item.direccion,
                        id: item.id
                    }
                })
            };
          },
          cache: true
        }
      });

  $("#laboratorio_id").change(function(){
    
});
  function reportepedidos()
  {
    //alert("me llamas");
    var from1 = document.getElementById("from1").value;
    var to1 = document.getElementById("to1").value;
    var usuario = document.getElementById("usuario").value;

    var entrega = document.getElementById("entrega").value;
    var pago = document.getElementById("pago").value;
    var hour = " 00:00:00"
    var from = from1.concat(hour);
    var to = to1.concat(hour);
    location.href="{{ url('/reportesPedidos?from=') }}"+ from + "&to="+to + "&usuario="+usuario+ "&entrega="+entrega+ "&pago="+pago;

  }

    function reportepedidosProveedor()
  {
    //alert("me llamas");
    var from1 = document.getElementById("from2").value;
    var to1 = document.getElementById("to2").value;
    var proveedor = document.getElementById("proveedor_id").value;
    var hour = " 00:00:00"
    var from = from1.concat(hour);
    var to = to1.concat(hour);
    location.href="{{ url('/reportesproveedores?from=') }}"+ from + "&to="+to + "&proveedor="+proveedor;

  }

 
  
</script>
  

           


          </div>

          <!-- /.col-lg-12 -->

      </div>



  </div>

@endsection
