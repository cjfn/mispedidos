
@extends('layouts.appprincipal')



@section('content')


<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container"  ng-app="myApp" ng-controller="myCtrl">
    <div class="row">
      <div class="col-md-2"></div>
          <div class="col-md-10">
            <div class="panel panel-default">
            <div class="panel panel-success">
                <div class="panel-heading"><strong>Detalle de pedido  No. {{Request()->id}} - cod. usuario: {{Request()->user_id}}  - Total {{$datadetallesp->total_productos}} productos - Descuento {{$datadetallesp->porcentaje_descuento}}%  {{$user->name}}</strong></div>


                  <input type="hidden" value=" {{Request()->id}}" name="pedido_id_no" id="pedido_id_no">



              </div>
            <div class="panel-body">
            <h2>Agrega codigo de producto a pedidos de la linea {{$datadescuento->nombre}} </h2>
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
                </div>
              @endif
              @if(!$datacount)
                <h3 style="color: red;">No hay productos agregados</h3>
                <button title="Es necesario que agregues un producto de lo contrario sera anulado automaticamente" type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#addModalDetalle"><span class="fa fa-plus"></span>Agrega un producto al pedido</button>
                
              @else
              <center>
               <span class="btn btn-warning btn-lg" onclick="printRpt('{{Request()->id}}')"> <span class="fa fa-print"></span> Imprimir</span>
              </center>       
        </center>
        <div >           
            
    <div class="table-responsive">
    <table class="table table-bordered" id="areaImprimir">
    <center>
      @if($datadetallesp->checkin==0)

      @else
         <button title="Es necesario que agregues un producto de lo contrario sera anulado automaticamente" type="button" class="btn btn-success" data-toggle="modal" data-target="#addModalDetalle"><span class="fa fa-plus"></span>Agrega un producto al pedido</button>
      @endif
   
    </center>
      <thead>
        <tr>
          <th>Opciones</th>
          <th><strong>Nombre producto</strong></th>
          <th>Talla</th>
          <th>color</th>
          <th>cantidad</th>
          <th>descuento</th>
          <th>precio venta</th>
          
        </tr>
      </thead>
      <tbody>
      @foreach($data as $x)
        <tr>
           <td>
             
              
             
              <button class="btn btn-danger" ng-click="deleteDetalle('{{$x -> id}}')" ><span class="fa fa-times-circle"></span>Quitar</button>
            @if( $x->precio_venta==0 &&  Auth::user()->tipo_usuario!='cliente')
              <button class="btn btn-success" data-toggle="modal" data-target="#viewModalCOnfirmaDetalle" onclick="fun_confirma('{{$x -> id}}')"><span class="fa fa-money"></span>Confirma precios</button>
            @else
              
            @endif

          </td>
          <td>{{$x -> codigo}} - {{$x -> nombre}}</td>
          <td>{{$x -> talla}}</td>
          <td>{{$x -> color}}</td>
          <td>{{$x -> cantidad}}</td>
          <td>{{$datadetallesp->porcentaje_descuento}}%</td>
          @if($x -> precio_venta==0)
          <td><strong title="comuniquese con su proveedor para darle mas seguimiento a su pedido">PENDIENTE DE CONFIRMAR</strong></td>
          @else
          <td>{{number_format($x -> precio_venta, 2)}}</td>
          @endif
          
        </tr>
       @endforeach
      </tbody>
    </table>


  <input type="hidden" name="" value="{{$total_devolucion=0}}">
  <input type="hidden" name="" value="{{$total_anticipo=0}}">
  <input type="hidden" name="" value="{{$total_pago=0}}">

              @foreach($bitacorapagos as $b)

              @if($b->tipo_pago=="ANTICIPO")

              <input type="hidden" name="" value="{{$total_anticipo=$b->pago+$total_anticipo}}">

              @elseif($b->tipo_pago=="DEVOLUCION")
              <input type="hidden" name="" value="{{$total_devolucion=$b->pago+$total_devolucion}}">
               @elseif($b->tipo_pago=="PAGO"||$B->tipo_pago=="ANTICIPO")
              <input type="hidden" name="" value="{{$total_pago=$b->pago+$total_pago}}">
             @else
             @endif
              @endforeach
               <p style="color:red; text-align: right;"> Devolucion:
                  <strong >+ Q.{{$total_devolucion}}</strong>
                </p> 
                 <p style="color:orange; text-align: right;"> Anticipo:
                  <strong >+ Q.{{$total_anticipo}}</strong>
                </p> 
              <input type="hidden" name="" value="{{$tpagado=0}}">

                
        <p style="color:green; text-align: right;" > Total Pagado:
              <strong >+ Q.{{number_format($total_pago
              , 2)}}</strong>
              </p>
              <p style="color:blue; text-align: right;" > Total:
              <strong >Q.{{$tpagado = number_format($datadetallesp->total, 2)}}</strong>
              </p>
              @if($datadetallesp->total!=$datadetallesp->total_pagado)
              <p style="color:red; text-align: right;">Pendientes Pago
               <strong>Q.{{$datadetallesp->total-$datadetallesp->total_pagado}}</strong> 
              </p>
              @else
              @endif

     <button class="btn btn-danger" title="Si cierra el pedido no podra agregar nuevos productos" data-toggle="modal" data-target="#viewModalDetalle" onclick="fun_edit('{{Request()->id}}')"><span class="fa fa-eye"></span>Cerrar Pedido de Producto</button>
     @endif

    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('detallespedidos/view')}}">
     <input type="hidden" name="hidden_delete_detalle" id="hidden_delete_detalle" value="{{url('detallespedidos/delete')}}">
    <!-- Add Modal start -->
    <div class="modal fade" id="addModalDetalle" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agregar nuevo detalle de producto de {{$datadescuento->nombre}}</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('detallespedidos') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <input type="hidden" class="form-control" id="user_id" name="user_id" required="true" value="{{Request()->user_id}}">

                  <input type="hidden" name="pedido_id" id="pedido_id" value="{{$pedido_id}}">
                </div>
                <div class="form-group">
                  <label for="last_name">Ingrese codigo de producto:</label>
                  <input type="text" class="form-control" id="codigo" name="codigo" title="este codigo esta en la revista de su proveedor favorito" required="true" tabindex="1">
                </div>
                <div class="form-group">
                  <label for="last_name">Nombre o descripcion del producto:</label>
                  <input type="text" class="form-control" id="nombre" name="nombre"  required="true" tabindex="2">
                </div>
                <div class="form-group">
                  <label for="last_name">Talla:</label>
                  <input type="text" class="form-control" id="talla" name="talla" title="solo si aplica para tipo de producto" required="true" tabindex="3">
                </div>
                 <div class="form-group">
                  <label for="last_name">Color:</label>
                  <input type="text" class="form-control" id="color" name="color" required="false" tabindex="4" title="solo si aplica de lo contrario dejar n/a">
                </div>
                 <div class="form-group">
                  <label for="last_name">cantidad:</label>
                  <input type="number" class="form-control" id="cantidad" name="cantidad" required="true" tabindex="5" title="Ingrese solo numeros">
                </div>
                  @if(Auth::user()->tipo_usuario=='cliente')
                   <div class="form-group">
                      <label for="last_name">Nombre de catalogo:</label>
                      <input type="text" class="form-control" id="catalogo" name="catalogo"  required="true"   tabindex="6" title="ejemplo: cklass">
                    </div>
                    <div class="form-group">
                      <label for="last_name">Numero de pagina:</label>
                      <input type="number" id="no_pagina" class="form-control"  name="no_pagina" title="ingrese solo el numero de la pagina" tabindex="7"> 
                    </div>
                    
                  @else
                     <div class="form-group">
                      <label for="last_name">Precio original:</label>
                      <input type="number" class="form-control" id="precio_costo" name="precio_costo"  required="true" onkeyup="calculatotal()"  tabindex="6" title="precio que viene en revista">
                    </div>
                    <div class="form-group">
                      <label for="last_name">Aplica Precio especial:</label>
                      <input type="checkbox" id="cbox1" value="first_checkbox" onclick="precioespecial()" tabindex="7"> 
                    </div>
                     <div class="form-group">
                      <label for="last_name">Precio de venta:</label>
                      <input type="text" readonly="readonly" class="form-control" id="precio_venta" name="precio_venta" required="true" tabindex="8">
                    </div>
                  @endif
                
              <input type="hidden" id="confirma_n_pedido" name="confirma_n_pedido">

              </div>
              
              <button type="submit" class="btn btn-default" tabindex="9">Agregar</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
    <!-- Print Modal start -->
    <div class="modal fade" id="viewModalDetalle" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Cerrar de pedido <span id="view_pedido_id"></span> </h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('pedidos/updateCerrar') }}" method="post">
        {{ csrf_field() }}        
        <center>
        <button type="submit"  class="btn btn-danger btn-lg">Cerrar Venta</button>   
        <span class="btn btn-success btn-
        lg" onclick="printRpt('{{Request()->id}}')">Imprimir</span>       
        </center>
       <pre id="areaImprimirD">
         <input type="hidden" id="id"
        name="id" value={{ $total = 0 }}>  
        <center>     
       DISTRIBUIDORA DONAJI
       <br/>
          <script type="text/javascript">
           var d = new Date();
            var month = new Array();
            month[0]="1";
            month[1]="2";
            month[2]="3";
            month[3]="4";
            month[4]="5";
            month[5]="6";
            month[6]="7";
            month[7]="8";
            month[8]="9";
            month[9]="10";
            month[10]="11";
            month[11]="12";
          var mes = month[d.getMonth()];



           document.write(d.getDate()+"/"+mes+"/"+d.getFullYear()+"  "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds());
           </script>
         <br/>
        PEDIDO PEDIDO NO. {{Request()->id}}      <br/>
        {{$user->name}}<br/>

        </center>
          -------------------------------------------------------------------------------------------------        
        <br/>
        <table style="border: solid 1px #000000; ">      
                  <tr>
                   
                    <th style="border: solid 1px #000000; "><strong>Nombre producto</strong></th>
                    <th style="border: solid 1px #000000; "> Talla</th>
                    <th style="border: solid 1px #000000; "> Color</th>
                    <th style="border: solid 1px #000000; "> Cantidad</th>
                    <th style="border: solid 1px #000000; "> Descuento %</th>
                    <th style="border: solid 1px #000000; "> Venta Q.</th>
                    
                  </tr>

                </thead>
                <tbody>
                @foreach($data as $x)
                  <tr>
                    <td style="border: solid 1px #000000; ">{{$x -> codigo}} - {{$x -> nombre}}</td>
                    <td style="border: solid 1px #000000; ">{{$x -> talla}}</td>
                    <td style="border: solid 1px #000000; ">{{$x -> color}}</td>
                    <td style="border: solid 1px #000000; ">{{$x -> cantidad}}</td>
                    <td style="border: solid 1px #000000; ">{{$datadetallesp->porcentaje_descuento}}%</td>
                    <td style="border: solid 1px #000000; ">{{number_format($x -> precio_venta, 2)}}</td>
                    
                  </tr>
                 @endforeach
                </tbody>
              </table>
              @foreach($bitacorapagos as $b)
              @if($b->id!=null)
                <h2 style="color:red; text-align: right;"> Devolucion:
                  <strong > Q.{{$b->pago}}</strong>
                </h2>
              @else
              @endif
            @endforeach 
                   -------------------------------------------------------------------------------------------------   
        <br/>
              <h3 style="color:blue; text-align: right;" > Total:
              <strong >Q.{{number_format($datadetallesp->total, 2)}}</strong>
              </h3>
              </pre>            
              </div>    
                <input type="hidden" name="edit_id" id="edit_id" value="{{Request()->id}} "> 
            </form>
        
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->

    
 
       <!-- Update Modal start -->
    <div class="modal fade" id="viewModalCOnfirmaDetalle" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Confirma precios de pedido <span id="view_pedido_id"></span> </h4>
          </div>
          <div class="modal-body">
              <form action="{{ url('detallespedidos/update') }}" method="post">
              {{ csrf_field() }}
              <input type="number" class="form-control" title="codigo unico de detalle pedido" readonly="readonly" id="confirma_pedido_id" name="confirma_pedido_id">
            <div class="form-group">
                 <div class="form-group" id="edit_obs">
                  <label for="edit_correo">Precio catalogo:</label>

              <input type="number" class="form-control"  min="0" value="0" step="0.01"  id="confirma_precio_costo" name="confirma_precio_costo">
                </div>
              <div class="form-group">
                 <div class="form-group" id="edit_obs">
                  <label for="edit_correo">Precio de venta con descuentos</label>
                  <input type="number" class="form-control"  min="0" value="0" step="0.01"  id="confirma_precio_venta" name="confirma_precio_venta">
                </div>
              <button type="submit" class="btn btn-default">Confirmar precios</button>
              <input type="hidden" id="edit_id_confirma" name="edit_id_confirma">

              <input type="hidden" id="confirma_n_pedido" name="confirma_n_pedido">


              </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>

 <!-- View Modal start -->
    <div class="modal fade" id="NviewModalDetalle" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Imprimir de pedido <span id="view_pedido_id"></span> </h4>
          </div>
          <div class="modal-body">
            < <form action="{{ url('detallespedidos/update') }}" method="post">
            {{ csrf_field() }}        
                <p><b>Nombre : </b><span id="view_nombre" class="text-success"></span></p>
            <p><b>codigo: </b><span id="view_codigo" class="text-success"></span></p>
            <p><b>talla : </b><span id="view_talla" class="text-success"></span></p>
            <p><b>color : </b><span id="view_color" class="text-success"></span></p>
            <p><b>cantidad : </b><span id="view_cantidad" class="text-success"></span></p>

            <p><b>precio costo : </b><span id="view_precio_costo" class="text-success"></span></p>
            <p><b>precio venta : </b><span id="view_precio_venta" class="text-success"></span></p>


            <p><b>catalogo : </b><span id="view_catalogo" class="text-success"></span></p>
            <p><b>numero de pagina : </b><span id="view_no_pagina" class="text-success"></span></p>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->

  </div>
  </div>
  </div>

 <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

                 
                 <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

   <script language="javascript" type="text/javascript">
                                    
                    $(document).ready(function () {
                      $('#MyTable').DataTable();

                       $('#addModal').on('shown.bs.modal', function() {
                        $('#codigo').focus();
                      });
                  } );



    function fun_delete(id)
    {
      var conf = confirm("Esta seguro que desea eliminar detalle??");
      if(conf){
        //var delete_url = $("#hidden_delete_detalle").val();
        var delete_url = "{{ url('/api/detallepedido/') }}";
        //alert(conf);
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            //alert(response);
            location.reload(); 
          }

        });
      }
      else{
          
      }
    }
    
  </script>



  <script type="text/javascript">

    
          var app = angular.module('myApp', []).controller('myCtrl', function ($scope, $http) { 

        


                    $scope.deleteDetalle = function(pedido){
                      alert("elimina detalle");
                      //alert("{{ url('/api/detallepedido/')}}"+"/"+pedido);

                      var conf = confirm("Esta seguro que desea eliminar detalle??");
                      if(conf){
                          $http.delete("{{ url('/api/detallepedido/')}}"+"/"+pedido).then(function (response) {
                                    location.reload();
                                })
                      }
                      else{
                          
                      }

                              
                     }              
                    });

        /*
        function precioespecial()
        {
           if (document.getElementById('cbox1').checked) {
              alert('Habilitando modificacion manual'); 
              document.getElementById("precio_venta").readOnly = false;
              } else {
                    alert('Deshabilitando modificacion manual'); 
                    document.gcd laravel
                    cd etElementById("precio_venta").readOnly = true;
              }
        }
        */

        function precioespecial()
          {
           if (document.getElementById('cbox1').checked) {
                alert('Habilitando modificacion manual'); 
                document.getElementById("precio_venta").readOnly = false;
          } else {
                alert('Deshabilitando modificacion manual'); 
                document.getElementById("precio_venta").readOnly = true;
          }
        }
   
 

         
        function calculatotal() {
          //alert("calculo");
          const formula = 0.5;
          var precio_costo = parseFloat(document.getElementById('precio_costo').value);
          var cantidad = (document.getElementById('cantidad').value);
          //alert(precio_costo+ " formula"+ formula);
          var descuento = parseFloat({{$datadetallesp->porcentaje_descuento}}/100);
          //alert(descuento);//0.3
          var total_pesos = parseFloat(precio_costo*formula);//(500*0.5)
          //alert(total_pesos);//250
          var a_q = parseFloat((precio_costo*0.5));//(500/0.5)*1
          //alert(a_q);//1000
          var precio_descuento = parseFloat(a_q*(descuento));
          //alert(precio_descuento);//300
          var precio_total = parseFloat(total_pesos-precio_descuento)*cantidad;
          //alert(precio_total);//-29750
          document.getElementById('precio_venta').value = precio_total;          
        }

    function printDiv(nombreDiv) {
      var contenido= document.getElementById(nombreDiv).innerHTML;
      var contenidoOriginal= document.body.innerHTML;
      document.body.innerHTML = contenido;
      window.print();
      document.body.innerHTML = contenidoOriginal;
    }

   

    function fun_detalle_view(id)
    {
      //alert(id);
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          console.log(result);
          //$("#view_user_id").text(result.user_id);

          $("#view_pedido_id").text(result.pedido_id);
          $("#view_codigo").text(result.codigo);
          $("#view_codigo_barras").text(result.codigo_barras);
          $("#view_nombre").text(result.nombre);
          $("#view_talla").text(result.talla);
          $("#view_color").text(result.color);
          $("#view_cantidad").text(result.cantidad);
          $("#view_precio_costo").text(result.precio_costo);
          $("#view_precio_venta").text(result.precio_venta);
          //$("#view_user_create").text(result.user_create);
          //$("#view_user_create_id").text(result.user_create_id);
          //$("#view_activo").text(result.activo);
          $("#view_catalogo").text(result.catalogo);
          $("#view_no_pagina").text(result.no_pagina);
          //$("#view_created_at").text(result.created_at);
          //$("#view_updated_at").text(result.updated_at);
        }
      });
    }
 
    function fun_edit(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#edit_id").val(id);
          $("#confirma_pedido_id").val(id);
          $("#confirma_precio_costo").val(precio_costo);
          $("#confirma_precio_venta").val(precio_venta);
          $("#confirma_n_pedido").val(result.pedido_id);
          $("#edit_id_confirma").val(id);
        }
      });
    }

     function fun_confirma(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          $("#edit_id_confirma").val(id);
          $("#confirma_pedido_id").val(id);
          $("#confirma_precio_costo").val(precio_costo);
          $("#confirma_precio_venta").val(precio_venta);
          $("#edit_id").val(result.id);
          $("#confirma_n_pedido").val(result.pedido_id);

         //alert(result.pedido_id);
         //alert(id);
        }
      });
    }

    function printRpt(id)
    {

    location.href="{{ url('/reportesdetallespedido?id=') }}"+ id;

    }

 </script>       
        </div>
          <!-- /.col-lg-12 -->
      </div>
</div>

@endsection

