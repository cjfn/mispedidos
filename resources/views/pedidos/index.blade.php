
@extends('layouts.appprincipal')



@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />



<div class="container">
    <div class="row">
      <div class="col-md-2"></div>
          <div class="col-md-10">
            <div class="panel panel-default">
            <div  style="background-image: linear-gradient(to bottom right, #75890c, #a4b357)">
                <div class="panel-heading" ><strong >TUS PEDIDOS</strong></div>
              </div>
            <div class="panel-body">
            <h3><strong>Administra pedidos de tus clientes a tus productos</strong></h3>
              @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
                </div>
              @endif
              <ul class="nav nav-tabs">
              <li class="active" title="crea pedidos de tus clientes"><a data-toggle="tab" href="#home"><span class="fa fa-hand-o-right"></span> 1.Ingresa Pedidos <span class="fa fa-search"></span> Busca</a></li> 
            
              <li title="Pedidos pendientes de entrega por proveedor"><a data-toggle="tab" href="#menu2"><span class="fa fa-chevron-right"></span> 2. Pendiente Proveedor </a></li>
              <li title="Pedidos disponibles en bodega"><a data-toggle="tab" href="#menu3"><span class="fa fa-cubes"></span> 3. En bodega</a></li> 
             
              <li title="Listado de pedidos entregados a tus clientes"><a data-toggle="tab" href="#menu4"><strong><span class="fa fa-check"></span>4. Entregados</strong></a></li> 
              </ul>
    <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
    <div class="table-responsive">
    <table class="table table-bordered" id="MyTable">
    <center>
    <button type="button" id="nPedido" name="nPedido" class="btn btn-success" data-toggle="modal" data-target="#addModal" tabindex="1"><span class="fa fa-plus"></span>Agrega Nuevo Pedido</button>
    </center>
      <thead>
        <tr>
          <th>Opciones</th>
          <th><strong>No. Pedido</strong></th>
          <th>cliente</th>
          <th>descuento</th>
          <th><span class="fa fa-truck"></span>Estado Entrega</th>
          <th>Estado pago</th>
          <th>Canal</th>
        </tr>
      </thead>
      <tbody>
      @foreach($dataColaPedido as $x)
        <tr>
           <th>
            <center title="Opciones de pedido No {{$x -> id}}">
    
              <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')"><span class="fa fa-eye"></span>Ver</button>
            
            @if(Auth::user()->tipo_usuario=='cliente' && $x -> estado_entrega!="PENDIENTE_PEDIDO")
            @else
            @if($x -> estado_entrega=="PENDIENTE_PEDIDO")
              <button title="Solicita pedido a tu proveedor, en su plataforma" class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}','solicitar','{{$x->estado_pago}}')"><span class="fa fa-pencil-square"></span>Solicitar </button>
            @else
               @if($x -> estado_entrega=="PENDIENTE_PEDIDO")
            

            @elseif($x->estado_entrega=="PEDIDO_PROVEEDOR")
              
              <a href="#" class="btn btn-default" onclick="fun_change('2')" title="para modificar opciones en la penstaña {{$x -> estado_entrega}}"> Cambio de estado </a>
            @elseif($x->estado_entrega=="PEDIDO_EN_SUCURSAL")
            <a href="#" class="btn btn-default" onclick="fun_change('3')" title="para modificar opciones en la penstaña {{$x -> estado_entrega}}"> Cambio de estado </a>
             @elseif($x->estado_entrega=="PEDIDO_ENTREGADO_CLIENTE")
            <a href="#" class="btn btn-default" onclick="fun_change('4')" title="para modificar opciones en la penstaña {{$x -> estado_entrega}}"> Cambio de estado </a>
            @elseif($x->estado_entrega=="PEDIDO_RECHAZADO_PARCIAMENTE")
             <STRONG style="color:red"><span class="fa fa-money"></span>PEDIDO RECHAZADO POR CLIENTE</STRONG>
             @elseif($x->estado_entrega=="PEDIDO_RECHAZADO_TOTALMENTE")
             <STRONG style="color:red"><span class="fa fa-money"></span>PEDIDO RECHAZADO</STRONG>
              @else
            @endif
             
            @endif
            @endif
            @if(Auth::user()->tipo_usuario=='cliente')
            @else
              <button class="btn btn-danger" title="eliminar pedidos" onclick="fun_delete('{{$x -> id}}')"><span class="fa fa-times-circle"></span>Cancelar</button>
            @endif
            </center>
          </th>
          <th><strong title="creado {{$x -> created_at}}"><center>{{$x -> id}} </strong><br></th>
          <th>{{$x -> user}}</th>
          <th>
            @if($x -> porcentaje_descuento>0)
            <strong style="color:green">SI {{$x->porcentaje_descuento}} %</strong>
            @else
            NO
            @endif
            </th>
          <th>
           @if($x -> estado_entrega=="PENDIENTE_PEDIDO")
            <strong style="color:red" title="comuniquese con la empresa para mas informacion"> <span class="fa fa-exclamation-circle"></span> PENDIENTE PEDIDO</strong>
            @elseif($x->estado_entrega=="PEDIDO_PROVEEDOR")
              <STRONG title="comuniquese con la empresa para mas informacion" style="color:#EACF26"><span class="fa fa-check"></span>PEDIDO A PROVEEDOR</STRONG>
            @elseif($x->estado_entrega=="PEDIDO_EN_SUCURSAL")
             <STRONG title="comuniquese con la empresa para mas informacion" style="color:orange"><span class="fa fa-check"></span>PEDIDO DISPONIBLE EN BODEGA</STRONG>
             @elseif($x->estado_entrega=="PEDIDO_ENTREGADO_CLIENTE")
             <STRONG style="color:green"><span class="fa fa-check"></span>PEDIDO ENTREGADO A CLIENTE</STRONG>
            @elseif($x->estado_entrega=="PEDIDO_RECHAZADO_PARCIAMENTE")
             <STRONG style="color:red"><span class="fa fa-money"></span>PEDIDO RECHAZADO POR CLIENTE</STRONG>
             @elseif($x->estado_entrega=="PEDIDO_RECHAZADO_TOTALMENTE")
             <STRONG style="color:red"><span class="fa fa-money"></span>PEDIDO RECHAZADO</STRONG>
              @else
            @endif
          </th>
           
            @if($x->estado_pago=="PAGO_TOTAL" && $x -> total!=0)
               <th>
              <p style="font-size: small;">
              <strong title="Total en Q. de pedido"> Q.{{number_format($x -> total,2)}}</strong> - <strong title="Total en Q. cancelado"> Q. {{number_format($x -> total_pagado,2)}}</strong>
            </p>
              <STRONG style="color:green"><span class="fa fa-money"></span>PAGO TOTAL
              
              </STRONG>
            </th>
            @else
            <th>
              @if(Auth::user()->tipo_usuario=='cliente')
              @else
              @if(number_format($x -> total,2)==0.00)
              <strong title="click en Ver y luego en Ver mas detalles para confirmar precios de pedido">Pendiente de confirmar precios de pedido</strong>
              @else
              <p style="font-size: x-small;">
              <strong title="Total en Q. de pedido"> Q.{{number_format($x -> total,2)}}</strong> - <strong title="Total en Q. cancelado"> Q. {{number_format($x -> total_pagado,2)}}</strong>
            </p>
            @if($x->total_pagado > 0)
             <STRONG style="color:orange"><span class="fa fa-money"></span>PENDIENTE DE PAGO {{number_format((($x -> total)-($x->total_pagado)),2)}}
            @else
              <STRONG style="color:red"><span class="fa fa-money"></span>PENDIENTE DE PAGO {{number_format($x -> total,2)}}
            @endif  
              <button title="Realiza pagos de tus clientes" class="btn btn-success" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}','pagar','{{$x->estado_pago}}')"><span class="fa fa-money"></span>Hacer pago </button>
              </STRONG>
              @endif
              @endif              
            </th>
            
            @endif
          <th>{{$x -> canal}}</td>
        </tr>
       @endforeach
      </tbody>
    </table>
  </div>
</div>

<div id="menu2" class="tab-pane fade">
  <h3>2. Pendientes de entrega por proveedor</h3>
  <p>Peidos con estado pendiente de entrega por parte de proveedor </p>
    <div class="table-responsive">
    <table class="table table-bordered" id="MyTable2">
    <center>
    
    </center>
      <thead>
        <tr>
          <th>Opciones</th>
          <th><strong>No. Pedido</strong></th>
          <th>cliente</th>
          <th>total Q</th>
          <th>descuento</th>
          <th><span class="fa fa-truck"></span>Estado Entrega</th>
          <th>Estado pago</th>
          <th>Canal</th>
        </tr>
      </thead>
      <tbody>
      @foreach($dataColaProveedor as $x)
        <tr>
           <td>
            <center title="Opciones de pedido No {{$x -> id}}">
              <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')"><span class="fa fa-eye"></span>Ver</button>
              <button title="Ingrese pedidos de proveedor a su bodega" class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}','ingresar','{{$x->estado_pago}}')"><span class="fa fa-sign-in"></span>Ingresar </button>
              <button class="btn btn-danger" title="Opcion desabilitada"><span class="fa fa-times-circle"></span>Cancelar</button>
            </center>
          </td>
          <td><strong title="creado {{$x -> created_at}}"><center>{{$x -> id}} </strong><br> <p>{{$x -> created_at}}</p></center></td>
          <td>{{$x -> user}}</td>
          <td title="descuento: {{$x->porcentaje_descuento}} %"><strong>Q. {{number_format($x -> total,2)}}</strong></td>
          <td>
            @if($x -> porcentaje_descuento>0)
            <strong style="color:green">SI {{$x->porcentaje_descuento}} %</strong>
            @else
            NO
            @endif
            </td>
          
            @if($x -> estado_entrega=="PENDIENTE_PEDIDO")
            <td><strong style="color:red"> <span class="fa fa-exclamation-circle"></span> PENDIENTE PEDIDO</strong></td>
            @elseif($x->estado_entrega=="PEDIDO_PROVEEDOR")
              <td><STRONG style="color:#EACF26"><span class="fa fa-check"></span>PEDIDO A PROVEEDOR</STRONG></td>
            @elseif($x->estado_entrega=="PEDIDO_EN_SUCURSAL")
              <td><STRONG style="color:orange"><span class="fa fa-check"></span>PEDIDO EN SUCURSAL </STRONG></td>
             @elseif($x->estado_entrega=="PEDIDO_ENTREGADO_CLIENTE")
              <td><STRONG style="color:green"><span class="fa fa-check"></span>PAGO TOTAL</STRONG></td>
            @elseif($x->estado_entrega=="PEDIDO_RECHAZADO_PARCIAMENTE")
              <td><STRONG style="color:red"><span class="fa fa-money"></span>PAGO TOTAL</STRONG></td>
             @elseif($x->estado_entrega="PEDIDO_RECHAZADO_TOTALMENTE")
              <td><STRONG style="color:red"><span class="fa fa-money"></span>PAGO TOTAL</STRONG></td>
              @else
            @endif
           
            @if($x->estado_pago=="PAGO_TOTAL")
               <td>
              <p style="font-size: small;">
              <strong title="Total en Q. de pedido"> Q.{{number_format($x -> total,2)}}</strong> - <strong title="Total en Q. cancelado"> Q. {{number_format($x -> total_pagado,2)}}</strong>
            </p>
              <STRONG style="color:green"><span class="fa fa-money"></span>PAGO TOTAL
              
              </STRONG>
            </td>
            @else
            <td>
              <p style="font-size: x-small;">
              <strong title="Total en Q. de pedido"> Q.{{number_format($x -> total,2)}}</strong> - <strong title="Total en Q. cancelado"> Q. {{number_format($x -> total_pagado,2)}}</strong>
            </p>
             @if($x->total_pagado > 0)
             <STRONG style="color:orange"><span class="fa fa-money"></span>PENDIENTE DE PAGO {{number_format((($x -> total)-($x->total_pagado)),2)}}
            @else
              <STRONG style="color:red"><span class="fa fa-money"></span>PENDIENTE DE PAGO {{number_format($x -> total,2)}}
            @endif  
              <button title="Realiza pagos de tus clientes" class="btn btn-success" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}','pagar','{{$x->estado_pago}}')"><span class="fa fa-money"></span>Hacer pago </button>
              </STRONG>
            </td>
            
            @endif
          <td>{{$x -> canal}}</td>
        </tr>
       @endforeach
      </tbody>
    </table>
  </div>
</div>

<div id="menu3" class="tab-pane fade">
  <h3><span class="fa fa-cubes"></span>3. En Bodega</h3>
  <p>Pedidos ya disponibles en la bodega principal</p>
  <div class="table-responsive">
     <table class="table table-bordered" id="MyTable">
    <center>
    
    </center>
      <thead>
        <tr>
          <th>Opciones</th>
          <th><strong>No. Pedido</strong></th>
          <th>cliente</th>
          <th>total Q</th>
          <th>descuento</th>
          <th><span class="fa fa-truck"></span>Estado Entrega</th>
          <th>Estado pago</th>
          <th>Canal</th>
        </tr>
      </thead>
      <tbody>
      @foreach($dataColaBodega as $x)
        <tr>
           <td>
            <center title="Opciones de pedido No {{$x -> id}}">
              <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')"><span class="fa fa-eye"></span>Ver</button>
              <button title="Entregar producto a destino" class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}','entregar','{{$x->estado_pago}}')"><span class="fa fa-car"></span>Entregar </button>
              <button class="btn btn-danger" title="Opcion desabilitada"><span class="fa fa-times-circle"></span>Cancelar</button>
            </center>
          </td>
          <td><strong title="creado {{$x -> created_at}}"><center>{{$x -> id}} </strong><br> <p>{{$x -> created_at}}</p></center></td>
          <td>{{$x -> user}}</td>
          <td title="descuento: {{$x->porcentaje_descuento}} %"><strong>Q. {{number_format($x -> total,2)}}</strong></td>
          <td>
            @if($x -> porcentaje_descuento>0)
            <strong style="color:green">SI {{$x->porcentaje_descuento}} %</strong>
            @else
            NO
            @endif
            </td>
          
         @if($x -> estado_entrega=="PENDIENTE_PEDIDO")
            <strong style="color:red"> <span class="fa fa-exclamation-circle"></span> PENDIENTE PEDIDO</strong>
            @elseif($x->estado_entrega=="PEDIDO_PROVEEDOR")
              <td><STRONG style="color:#EACF26"><span class="fa fa-check"></span>PEDIDO A PROVEEDOR</STRONG></td>
            @elseif($x->estado_entrega=="PEDIDO_EN_SUCURSAL")
              <td><STRONG style="color:orange"><span class="fa fa-check"></span>PEDIDO EN SUCURSAL</STRONG></td>
             @elseif($x->estado_entrega=="PEDIDO_ENTREGADO_CLIENTE")
              <td><STRONG style="color:green"><span class="fa fa-check"></span>PAGO TOTAL</STRONG></td>
            @elseif($x->estado_entrega=="PEDIDO_RECHAZADO_PARCIAMENTE")
              <td><STRONG style="color:red"><span class="fa fa-money"></span>PAGO TOTAL</STRONG></td>
             @elseif($x->estado_entrega=="PEDIDO_RECHAZADO_TOTALMENTE")
              <td><STRONG style="color:red"><span class="fa fa-money"></span>PAGO TOTAL</STRONG></td>
              @else
            @endif
            
            @if($x->estado_pago=="PAGO_TOTAL")
               <td>
              <p style="font-size: small;">
              <strong title="Total en Q. de pedido"> Q.{{number_format($x -> total,2)}}</strong> - <strong title="Total en Q. cancelado"> Q. {{number_format($x -> total_pagado,2)}}</strong>
            </p>
              <STRONG style="color:green"><span class="fa fa-money"></span>PAGO TOTAL
              
              </STRONG>
            </td>
            @else
            <td>
              <p style="font-size: x-small;">
              <strong title="Total en Q. de pedido"> Q.{{number_format($x -> total,2)}}</strong> - <strong title="Total en Q. cancelado"> Q. {{number_format($x -> total_pagado,2)}}</strong>
            </p>
              @if($x->total_pagado > 0)
             <STRONG style="color:orange"><span class="fa fa-money"></span>PENDIENTE DE PAGO {{number_format((($x -> total)-($x->total_pagado)),2)}}
            @else
              <STRONG style="color:red"><span class="fa fa-money"></span>PENDIENTE DE PAGO {{number_format($x -> total,2)}}
            @endif  
            
              <button title="Realiza pagos de tus clientes" class="btn btn-success" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}','pagar','{{$x->estado_pago}}')"><span class="fa fa-money"></span>Hacer pago </button>
              </STRONG>
            </td>
            
            @endif
          <td>{{$x -> canal}}</td>
        </tr>
       @endforeach
      </tbody>
    </table>
  </div>
</div>
<div id="menu4" class="tab-pane fade">
 <h3><span class="fa fa-send"></span>4. Entregados a cliente vendedores</h3>
  <p>Pedidos entregados al cliente final</p>
  <div class="table-responsive">
    <table class="table table-bordered" id="MyTable">
    <center>
    
    </center>
      <thead>
        <tr>
          <th>Opciones</th>
          <th><strong>No. Pedido</strong></th>
          <th>cliente</th>
          <th>total Q</th>
          <th>descuento</th>
          <th><span class="fa fa-truck"></span>Estado Entrega</th>
          <th>Estado pago</th>
          <th>Canal</th>
        </tr>
      </thead>
      <tbody>
      @foreach($dataColaEntregados as $x)
        <tr>
           <td>
            <center title="Opciones de pedido No {{$x -> id}}">
              <button class="btn btn-info" data-toggle="modal" data-target="#viewModal" onclick="fun_view('{{$x -> id}}')"><span class="fa fa-eye"></span>Ver</button>
             
              <button class="btn btn-danger" title="Opcion desabilitada"><span class="fa fa-times-circle"></span>Cancelar</button>
            </center>
          </td>
          <td><strong title="creado {{$x -> created_at}}"><center>{{$x -> id}} </strong><br> <p>{{$x -> created_at}}</p></center></td>
          <td>{{$x -> user}}</td>
          <td title="descuento: {{$x->porcentaje_descuento}} %"><strong>Q. {{number_format($x -> total,2)}}</strong></td>
          <td>
            @if($x -> porcentaje_descuento>0)
            <strong style="color:green">SI {{$x->porcentaje_descuento}} %</strong>
            @else
            NO
            @endif
            </td>
          
         @if($x -> estado_entrega=="PENDIENTE_PEDIDO")
            <td><strong style="color:red"> <span class="fa fa-exclamation-circle"></span> PENDIENTE PEDIDO</strong></td>
            @elseif($x->estado_entrega=="PEDIDO_PROVEEDOR")
              <td><STRONG style="color:#EACF26"><span class="fa fa-check"></span>PEDIDO A PROVEEDOR</STRONG></td>
            @elseif($x->estado_entrega=="PEDIDO_EN_SUCURSAL")
              <td><STRONG style="color:orange"><span class="fa fa-check"></span>PEDIDO EN SUCURSAL</STRONG></td>
             @elseif($x->estado_entrega=="PEDIDO_ENTREGADO_CLIENTE")
              <td><STRONG style="color:green"><span class="fa fa-check"></span>ENTREGADO A CLIENTE</STRONG></td>
            @elseif($x->estado_entrega=="PEDIDO_RECHAZADO_PARCIAMENTE")
              <td><STRONG style="color:red"><span class="fa fa-money"></span>PAGO TOTAL</STRONG></td>
             @elseif($x->estado_entrega=="PEDIDO_RECHAZADO_TOTALMENTE")
              <td><STRONG style="color:red"><span class="fa fa-money"></span>PAGO TOTAL</STRONG></td>
              @else
            @endif
            
            @if($x->estado_pago=="PAGO_TOTAL")
               <td>
              <p style="font-size: small;">
              <strong title="Total en Q. de pedido"> Q.{{number_format($x -> total,2)}}</strong> - <strong title="Total en Q. cancelado"> Q. {{number_format($x -> total_pagado,2)}}</strong>
            </p>
              <STRONG style="color:green"><span class="fa fa-money"></span>PAGO TOTAL
              
              </STRONG>
            </td>
            @else
            <td>
              <p style="font-size: x-small;">
              <strong title="Total en Q. de pedido"> Q.{{number_format($x -> total,2)}}</strong> - <strong title="Total en Q. cancelado"> Q. {{number_format($x -> total_pagado,2)}}</strong>
            </p>
             @if($x->total_pagado > 0)
             <STRONG style="color:orange"><span class="fa fa-money"></span>PENDIENTE DE PAGO {{number_format((($x -> total)-($x->total_pagado)),2)}}
            @else
              <STRONG style="color:red"><span class="fa fa-money"></span>PENDIENTE DE PAGO {{number_format($x -> total,2)}}
            @endif  
            
              <button title="Realiza pagos de tus clientes" class="btn btn-success" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$x -> id}}','pagar','{{$x->estado_pago}}')"><span class="fa fa-money"></span>Hacer pago </button>
              </STRONG>
            </td>
            
            @endif
           
          <td>{{$x -> canal}}</td>
        </tr>
       @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>
    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('pedidos/view')}}">
    <input type="hidden" name="hidden_view_usuarios" id="hidden_view_usuarios" value="{{url('usuarios/view')}}">

    <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('pedidos/delete')}}">
    <input type="hidden" name="hidden_update" id="hidden_update" value="{{url('pedidos/update')}}">
    
    <!-- Add Modal start -->
    <div class="modal fade" id="addModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agregar nuevo Pedido</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('pedidos') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">Seleccione cliente:</label>
                  
                  <select id="user_id"  name="user_id" title="seleccione cliente" required="true" class="form-control" tabindex="1" >

                  <option value="">Seleccione un usuario cliente ...</option>
                      @foreach($usuarios as $d)
                  
                      <option value="{{ $d -> id}}"> {{ $d -> name}} - <strong> {{ $d -> tipo_entrega}}
                        @if($d->tipo_usuario=='cliente')
                       Descuento: {{ $d -> descuento}} %
                       @else
                       No aplica descuento
                       @endif
                     </strong> </option>
                  
                  @endforeach
                   </select>
                </div>
                <div class="form-group">
                  <label for="last_name">seleccione proveedor:</label>
                   <select id="proveedor_id"  name="proveedor_id" title="seleccione cliente" required="true" class="form-control" tabindex="2">

                  <option value="">Seleccione proveedor ...</option>
                      @foreach($proveedores as $d)
                  
                      <option value="{{ $d -> id}}"> {{ $d -> nombre}} </option>
                  
                  @endforeach
                   </select>
                </div>
                @if(Auth::user()->tipo_usuario=='cliente')
                 <div class="form-group">
                  <label for="last_name">descuento:</label>
                  <input type="number" title="si 25% entonces ingrese solo 25" class="form-control" id="porcentaje_descuento" value="{{Auth::user()->descuento}}" name="porcentaje_descuento" required="false" readonly="readonly">
                </div>
                @else
                   
                @endif
               
                 
                 <div class="form-group">
                  <label for="last_name">canal:</label>
                  <select id="canal"  name="canal" title="seleccione medio de resepcion envio" tabindex="4"required="true"  class="form-control">
                    <option value="PERSONAL">PERSONAL</option>
                    <option value="TELEFONO">TELEFONO</option>
                    <option value="WATSAPP">WATSAPP</option>
                    <option value="RED_SOCIAL">RED SOCIAL</option>
                  </select>
                 
                </div>
                <div class="form-group">
                  <label for="last_name">tipo de pago:</label>
                  <select id="tipo_pago"  name="tipo_pago" title="seleccione tipo de pago (EFECTIVO, CHEQUE, DEPOSITO, OTROS" required="true"  class="form-control" tabindex="5">
                    <option value="EFECTIVO">EFECTIVO</option>
                    <option value="DEPOSITO">DEPOSITO</option>
                    <option value="CHEQUE">CHEQUE</option>
                    <option value="OTROS" title="Especifique en observaciones medio de pago">OTROS</option>
                  </select>
                 
                </div>

                <div class="form-group">
                  <label for="last_name">observaciones:</label>
                  <input type="text" class="form-control" required="false" id="observaciones" name="observaciones" tabindex="6">
                </div>
              </div>
              
              <button type="submit" tabindex="7" class="btn btn-default">Agregar</button>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-window-close"></span>Cerrar</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- add code ends -->
 
    <!-- View Modal start -->
    <div class="modal fade" id="viewModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalles de pedido <span id="view_id" class="text-success"></span></h4>
          </div>
          <div class="modal-body">
            <p><b>cliente : </b> cod:<span id="view_user_id" class="text-success"> - </span> <span id="view_user" class="text-success"></span></p>
            <p><b>Cod. Proveedor : </b><span id="view_proveedor_id" class="text-warning"></span></p>
            <p><b>Total  Pedido : <strong> (Q) <span id="view_total" class=""></span></strong></b></p>
            <p><b>Total  Pagado : </b><strong> (Q) <span id="view_total_pagado" class=""></span></strong> </p>
           Total descuento : </b><span id="view_porcentaje_descuento" class=""></span>%</b></p>
            <p><b>Estado entrega : </b><span id="view_estado_entrega" class=""></span></p>
            <p><b>Estado pago : </b><span id="view_estado_pago" ></span></b></p>
            <p><b>Medio recibido : </b><span id="view_canal" ></span></b></p>

            <p><b>Creado por : </b><span id="view_user_create" ></span></b></p>
            <p><b>Fecha creación : </b><span id="view_created_at" ></span></b></p>

            <p><b>Fecha modificación : </b><span id="view_updated_at" ></span></b></p>
            <p><b>Observaciones: <span id="view_observaciones" class="text-warning"></span></b></p>
          </div>
          <span class="btn btn-info" onclick="detallesPeido()"><span class="fa fa-eye"></span>Ver mas detalles</span>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"></button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- view modal ends -->
 
    <!-- Edit Modal start -->

    <div class="modal fade" id="editModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Cambiar estado a <span id="new_status"></span> </h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('pedidos/update') }}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                 <div class="form-group" id="edit_obs">
                  <label for="edit_correo">Hacer Pago en Q.</label>
                  <input type="number" class="form-control" step="0.01" id="edit_pago" name="edit_pago">
                </div>
                <div class="form-group" id="edit_obs">
                  <label for="edit_correo">Tipo de pago</label>
                   <select id="edit_tipo_pago"  name="edit_tipo_pago" title="seleccione tipo de pago (PAGO NORMAL O ANTICIPO" required="true"  class="form-control" tabindex="5">
                    <option value="PAGO">PAGO</option>
                    <option value="ANTICIPO">ANTICIPO</option>
                    <option value="DEVOLUCION">DEVOLUCION</option>
                  </select>
                </div>
                 <div class="form-group">
                  <label for="edit_correo">observaciones :</label>
                  <input type="text" class="form-control" id="edit_observaciones" name="edit_observaciones">
                </div>               
              </div>
              <input type="hidden" name="edit_estado_entrega" id="edit_estado_entrega" >

              <button type="submit" class="btn btn-default">Cambiar estado</button>
              <input type="hidden" id="edit_id" name="edit_id">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
          
        </div>
        
      </div>
    </div>
    <!-- Edit code ends -->
 
  </div>
  </div>
  </div>

 <link href="{{ url('/') }}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

                 
                 <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
                <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>

                <script language="javascript" type="text/javascript">
                                    
                  
                    $(document).ready(function () {
                      $('#MyTable').DataTable({
                         "order": [[ 1, "desc" ]]
                      });
                      $('#MyTable2').DataTable();


                       $('#addModal').on('shown.bs.modal', function() {
                        $('#nombre').focus();
                        $('#user_id').focus();
                      });
                  } );
                </script>



  <script type="text/javascript">

    function buscaDescuento(){
     
       // var select = document.getElementById("user_id");
        var id = document.getElementById("user_id").selectedIndex; 
        //alert(id);
        var view_url_u = $("#hidden_view_usuarios").val();
        //alert(view_url_u);
      $.ajax({
        url: view_url_u,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          console.log(result);
          var result=result;
                              
          $("#porcentaje_descuento").text(result.descuento);
          
        }
      });
      }

     function detallesPeido()
      {
        //alert("me llamas");
        var id = document.getElementById("view_id").innerHTML;
        var user_id = document.getElementById("view_user_id").innerHTML;
        location.href="{{ url('/detallespedidos?id=') }}"+ id + "&user_id="+user_id;

      }
      function fun_change(opcion)
      {
        alert("Para cambiar estado, selecciona la opcion No. "+opcion);
      }

    function fun_view(id)
    {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          console.log(result);
          var result=result;
                              

          $("#view_id").text(result.id);
          $("#view_user_id").text(result.user_id);
          $("#view_user").text(result.user);
          $("#view_total").text(Number(result.total).toFixed(2));
          $("#view_total_pagado").text(Number(result.total_pagado).toFixed(2));
          $("#view_aplica_descuento").text(result.aplica_descuento);
          $("#view_porcentaje_descuento").text(result.porcentaje_descuento);
          $("#view_estado_entrega").text(result.estado_entrega);
          $("#view_estado_pago").text(result.estado_pago);
          $("#view_observaciones").text(result.observaciones);
          $("#view_created_at").text(result.created_at);
          $("#view_updated_at").text(result.updated_at);
          $("#view_user_create").text(result.user_create);
          $("#view_proveedor_id").text(result.proveedor_id);
          $("#view_canal").text(result.canal);
        }
      });
    }
 
    function fun_edit(id, tipo_modificacion, estado_pago)
    {

      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          //console.log(result);
          if(tipo_modificacion=='solicitar')
          {
            //alert('solicitar');
            document.getElementById('edit_estado_entrega').value = "PEDIDO_PROVEEDOR";
            document.getElementById('new_status').innerHTML = tipo_modificacion;
            document.getElementById("edit_obs").style.display = "none";
            $("#edit_id").val(result.id);
           
            

          }
          else if(tipo_modificacion=='ingresar')
         {
            //alert('ingresar');
            document.getElementById('edit_estado_entrega').value = "PEDIDO_EN_SUCURSAL"; 
            document.getElementById('new_status').innerHTML = tipo_modificacion;
            document.getElementById("edit_obs").style.display = "none";
            $("#edit_id").val(result.id);
          }
          else if(tipo_modificacion=='pagar')
         {
            //alert('pagar');
            document.getElementById('edit_estado_entrega').value = tipo_modificacion; 
            document.getElementById('new_status').innerHTML = tipo_modificacion;
            document.getElementById("edit_obs").style.display = "inline";
            
            $("#edit_id").val(result.id);
          }
           else if(tipo_modificacion=='entregar')
         {
            //alert('pagar');

            if(estado_pago!='PAGO_TOTAL')
            {
              var conf = confirm("dese hacer una entrega sin pago completo");
              if(!confirm)
              {
                alert("no pues no");

                 $('#editModal').modal('hide');
                  location.reload();  
              }
              else
              {
                document.getElementById('edit_estado_entrega').value = "PEDIDO_ENTREGADO_CLIENTE"; 
                document.getElementById('new_status').innerHTML = "PEDIDO_ENTREGADO_CLIENTE";
                document.getElementById("edit_obs").style.display = "none";
                $("#edit_id").val(result.id);
                document.getElementById("edit_obs").style.display = "none";
              }
            }
            else
            {
                 document.getElementById('edit_estado_entrega').value = "PEDIDO_ENTREGADO_CLIENTE"; 
                document.getElementById('new_status').innerHTML = "PEDIDO_ENTREGADO_CLIENTE";
                document.getElementById("edit_obs").style.display = "none";
                $("#edit_id").val(result.id);
                document.getElementById("edit_obs").style.display = "none";
            }
            
          }


        }
      });
    }
 
    function fun_delete(id)
    {
      var conf = confirm("Esta seguro que desea eliminar??");
      if(conf){
        var delete_url = $("#hidden_delete").val();
        $.ajax({
          url: delete_url,
          type:"POST", 
          data: {"id":id,_token: "{{ csrf_token() }}"}, 
          success: function(response){
            //alert(response);
            location.reload(); 
          }
        });
      }
      else{
        return false;
      }
    }
 </script>       
          </div>
          <!-- /.col-lg-12 -->
      </div>
</div>

@endsection

