@extends('layouts.appprincipal')

@section('content')
    
<div class="container">

    <div class="row">
          <div class="col-md-8 col-md-offset-2">
                    <h1><span class="fa fa-lightbulb-o"></span> <span class="text-success"></span>Sistema de pedidos Distribuidora Donají</strong></h1>
                </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
              

                <div class="panel-body">
                    <strong> Bienvenido, en esta app podras tener el control de todo lo relacionado con tus pedidos</strong>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
